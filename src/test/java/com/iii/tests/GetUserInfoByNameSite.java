package com.iii.tests;


import org.json.JSONObject;
import com.iii.common.UtilityTools;
import org.junit.Assert;
import com.iii.resources.User;
@SuppressWarnings("unused")

public class GetUserInfoByNameSite {
	
	public static boolean test(String site, String envName) throws Exception {
		
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String userInfoUrl = site + "users?username=" + username + "&sitecode=" + libsite;
		
		try {
			// getting user info
				String myStringUserInfo = UtilityTools.streamStringUrl(userInfoUrl, envName);

			// creating User JSONObject from JSON String
				JSONObject myUserObject = new JSONObject(myStringUserInfo);
				User MyUser = new User (myUserObject);
				Assert.assertNotNull(MyUser);

				
			} catch (Exception e) {
				success = false;
				System.err.println("Unable to creating User object" + e.toString() + "\n");
	
			}
	// Verification username not found at site - 404
		
		userInfoUrl = site + "users?username=aaa" + "&sitecode=" + libsite;
		String myStringUserInfo = UtilityTools.streamStringUrl(userInfoUrl, envName);
		Assert.assertEquals("404", myStringUserInfo.substring(0, 3));
	
	// Verification user not authorized - 403
		
		userInfoUrl = site + "users?username=cwcat" + "&sitecode=" + libsite;
		myStringUserInfo = UtilityTools.streamStringUrl(userInfoUrl, envName);
		Assert.assertEquals("403", myStringUserInfo.substring(0, 3));


		
		return success;
	}

}
