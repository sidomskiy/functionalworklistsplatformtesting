package com.iii.tests;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.Random;
import org.junit.Assert;
import org.junit.Test;

import com.iii.common.UtilityTools;
@SuppressWarnings("unused")

public class GetItemSummary {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		int itemWorklistId = CreateNewItemWorklist.test(site, envName);

		String itemBarcode = getItemBarcode(itemWorklistId, envName);

		String requestUrl = site + "itemsummary?itemBarcode="+ itemBarcode;
		
		String response = UtilityTools.streamStringUrl(requestUrl, envName);
		
		JSONObject ItemSumm = new JSONObject(response);
		
		try {
			Assert.assertEquals(itemBarcode, ItemSumm.getString("barcode"));
			
		} catch (Exception e) {
			success = false;
			e.printStackTrace();

		}
		
		return success;
	}
	
	public static String getItemBarcode(int itemWorklistId, String envName) throws Exception{
		String itemBarcode = null;
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		String site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String requestUrl = site + "itemworklists/"+ itemWorklistId;
		
		String itemWorklist = UtilityTools.streamStringUrl(requestUrl, envName);
		
		JSONObject JSONWorklists = new JSONObject(itemWorklist);
		JSONArray itemWorkList = JSONWorklists.getJSONArray("entries");
		// create random object 
        Random ran = new Random();
        int nxt = ran.nextInt(itemWorkList.length()); 
		JSONObject childItemObject = itemWorkList.getJSONObject(nxt);
		itemBarcode = childItemObject.getString("barcode");
		return itemBarcode;
	}

}
