package com.iii.tests;


import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.iii.common.UtilityTools;
@SuppressWarnings("unused")

public class GetLibrarieBySitecode {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String getLibrarieUrl = site + "sites?sitecode=" + libsite;
		String response = UtilityTools.streamStringUrl(getLibrarieUrl, envName);
		JSONObject JSONWorklists = new JSONObject(response);
		
		try {
			Assert.assertEquals(libsite, JSONWorklists.getString("siteCode"));
			
		} catch (Exception e) {
			success = false;
			e.printStackTrace();

		}
		return success;
	}

}
