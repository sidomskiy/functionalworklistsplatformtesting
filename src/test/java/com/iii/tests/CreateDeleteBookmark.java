package com.iii.tests;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONArray;
import java.util.Random;
import com.iii.common.UtilityTools;
import org.junit.Assert;
import org.junit.Test;
@SuppressWarnings("unused")

public class CreateDeleteBookmark {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		int listId = CreateNewItemWorklist.test(site, envName);
	// create bookmark
		String requestUrl = site + "itemworklists/" + listId + "/bookmarks";
		String itemId = getItemId(site, listId, envName);
		JSONObject newBookmark = new JSONObject();
		newBookmark.put("bookmarkId", itemId);
		String post_json = newBookmark.toString();
		
		
		String NewItmBookmark =  streamUrlPost(requestUrl, post_json, envName);
		String newBookId = getBookmark(site, listId, envName);
		
		try {
			Assert.assertEquals(newBookId, itemId);
			
		} catch (Exception e) {
			success = false;
			e.printStackTrace();

		}
	// delete bookmark
		String deleteUrl = site + "itemworklists/" + listId + "/bookmarks/"+ itemId;
		int delResult = UtilityTools.streamUrlDelete(deleteUrl, envName);
		newBookId = getBookmark(site, listId, envName);
		
		try {
			Assert.assertEquals("null", newBookId);
			
		} catch (Exception e) {
			success = false;
			e.printStackTrace();

		}
		
		
		return success;
	}
	
	public static String getItemId(String site, int itemWorklistId, String envName) throws Exception{
		String itemId = null;
		String requestUrl = site + "itemworklists/"+ itemWorklistId;
		
		String itemWorklists = UtilityTools.streamStringUrl(requestUrl, envName);
		//System.out.println("result: " + itemWorklists);
		JSONObject JSONWorklists = new JSONObject(itemWorklists);
		JSONArray itemWorkList = JSONWorklists.getJSONArray("entries");
		// create random object 
        Random ran = new Random();
        int nxt = ran.nextInt(itemWorkList.length()); 
		JSONObject childItemObject = itemWorkList.getJSONObject(nxt);
		//itemId = childItemObject.getString("recordNumber");
		//itemId = itemId.substring(1);
		
		itemId = Integer.toString(childItemObject.getInt("id"));
		return itemId;
	}
	
	public static String getBookmark (String site, int listId, String envName) throws Exception{
		String bookmarkId = null;
		
		String requestUrl = site + "itemworklists/"+ listId;
		
		String itemWorklists = UtilityTools.streamStringUrl(requestUrl, envName);
		JSONObject ItemWorklist = new JSONObject (itemWorklists);
		bookmarkId = ItemWorklist.get("bookmark").toString();
		
		return bookmarkId;
	}
	
	public static String streamUrlPost (String urlString, String json_string, String testSite) throws Exception {
		String inputStr = null;
		try {
			StringRequestEntity requestEntity = new StringRequestEntity(json_string, "application/json", "UTF-8");
			HttpClient client = new HttpClient();
			PostMethod postApiResp = new PostMethod(urlString);
			postApiResp.addRequestHeader("Authorization", ("Bearer " + UtilityTools.getAuthenticationToken(testSite)));
			postApiResp.setRequestEntity(requestEntity);
			int returnCode = client.executeMethod(postApiResp);
			if (returnCode == 204){
				
				inputStr="Result: success - " + returnCode + "\n";
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
				inputStr=returnCode + " | "+ UtilityTools.readAll(br);
			}
		} catch (Exception e) {
			System.out.println("error from Utility tool: " + e.toString() + "\n");
			e.printStackTrace();
			inputStr = e.toString();

		}
		return inputStr;
	}
	
	

}
