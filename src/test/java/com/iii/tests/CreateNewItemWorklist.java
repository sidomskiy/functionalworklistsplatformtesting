package com.iii.tests;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.JSONObject;
import org.junit.Test;

import com.iii.common.*;
@SuppressWarnings("unused")

public class CreateNewItemWorklist {
	
	@Test
	public static int test(String site, String envName) throws Exception {
		//public void test() throws Exception {
			boolean success = true;
			long sleepTime = 20;
			
			JSONObject envInfo = UtilityTools.getEnvInfo(envName);
			 
			site = envInfo.getString("site");
			String oathsite = envInfo.getString("oathsite");
			String Thrifthost= envInfo.getString("Thrifthost");
			String username = envInfo.getString("username");
			String password = envInfo.getString("password");
			String libsite = envInfo.getString("libsite");
					
			
			String newItemWorklist = site + "itemworklists/";
			int worklistId;
			
			String id = getUserId(site, envName, libsite, username);
			//System.out.print("user id: " + id + "\n");
			// Create a New Item Worklist
			String itemWorklistName = "Serge qa Test worklist "+ getDateTime();
			JSONObject newItmWkl = new JSONObject();
			newItmWkl.put("userId", id);
			 newItmWkl.put("name", itemWorklistName);
			 
			 String post_json = newItmWkl.toString();
			
			 
			 JSONObject NewItmWkl =  UtilityTools.streamJObjectPost(newItemWorklist, post_json, envName);
			 //System.out.print(NewItmWkl.toString() + "\n");
			 int newId = NewItmWkl.getInt("id");
			 String itemWorklistId = Integer.toString(newId);
			 String barcodes = FindBibsBarcode.test(envName);
			 String[] barcodeNums = barcodes.split(", ");
				
				for (int i=0; i< barcodeNums.length; i++){
					// Creating a addItemWorklist json
						
						JSONObject obj=new JSONObject();
							
					// add barcode	
						obj.put("barcode", barcodeNums[i]);
						
					// Creating json string
						 
						String json_string = obj.toString();
					        
					// adding worklist id to url
					    
					    String myAddItemToWorklist = newItemWorklist + itemWorklistId + "/items";
					    
					        
					// Adding an item-entry to a worklist
					     
						String addItemEntryWorklist = UtilityTools.streamUrlPost(myAddItemToWorklist, json_string, envName);
						UtilityTools.MySleep(sleepTime);	
					   // System.out.println(i+ ": " +addItemEntryWorklist + "\n");
				 }
			
			 
	        return newId;
			
		}
		
		public static String getDateTime() {
			String dateFormatNow = "HHmmssSSS";
			String dateTimeStr = null;
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormatNow);
			dateTimeStr = sdf.format(cal.getTime());
			return dateTimeStr;
		}
		
		public static String getUserId (String site, String testSite, String site_id, String username) throws Exception {
			String myUserId = "";
			String myUserInfoUrl = site + "users?sitecode="+site_id+"&username="+username;
			//System.out.print(myUserInfoUrl + " | " + testSite + "\n");
			String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, testSite);
			//System.out.print("User info" + myStringAllUsersJson + "\n");
			JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
			
			myUserId = myUserInfoObject.optString("id");
			
			return myUserId;
		}

	}