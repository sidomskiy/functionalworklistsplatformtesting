package com.iii.tests;


import org.junit.Assert;
import org.junit.Test;
import org.json.JSONObject;
import com.iii.common.UtilityTools;
import com.iii.resources.User;
@SuppressWarnings("unused")
public class GetUserInfoById {

	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String userId = getUserId(site, envName, libsite, username);

		
	// Verifying get user info by id
		
		String userInfoByIdUrl = site + "users/" + userId;
		String userInfoById = UtilityTools.streamStringUrl(userInfoByIdUrl, envName);

		JSONObject myUserObject = new JSONObject(userInfoById);
		
		try {
			User MyUser = new User (myUserObject);
			Assert.assertNotNull(MyUser);
		} catch (Exception e) {
			success = false;
			System.err.println("Unable to creating User object " + e.toString() + "\n");

		}
		
		// Verifying user not found in worklists database - 404
	
		 userId = "asdasd";
		 userInfoByIdUrl = site + "users/" + userId;
		 userInfoById = UtilityTools.streamStringUrl(userInfoByIdUrl, envName);
		 Assert.assertEquals("404", userInfoById.substring(0, 3));
		 
	  
		return success;
	}
	
	public static String getUserId (String site, String testSite, String site_id, String username) throws Exception {
		String myUserId = "";
		String myUserInfoUrl = site + "users?sitecode="+site_id+"&username="+username;

		String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, testSite);

		JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
		
		myUserId = myUserInfoObject.optString("id");
		
		return myUserId;
	}

}
