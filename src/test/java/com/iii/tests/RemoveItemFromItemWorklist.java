package com.iii.tests;


import org.junit.Assert;
import org.junit.Test;
import org.json.JSONArray;
import org.json.JSONObject;
import com.iii.common.UtilityTools;
@SuppressWarnings("unused")

public class RemoveItemFromItemWorklist {
	
	static int itemId = 2;
	static boolean success = true;
	
	@Test
	public static boolean test(String site, String testSite) throws Exception {
		//public void test() throws Exception {
			
			JSONObject getEnvInfo = UtilityTools.getEnvInfo(testSite);
			site = getEnvInfo.getString("site");
			String Thrifthost = getEnvInfo.getString("Thrifthost");
			String oathsite = getEnvInfo.getString("oathsite");
			String username = getEnvInfo.getString("username");
			String password = getEnvInfo.getString("password");
			String site_id = getEnvInfo.getString("libsite");
			
			String id = getUserId(site, testSite, site_id, username);
			
			int itemWorklistId = getItemWorklistId(site, id, testSite);

			itemId = getItemId(site, testSite, itemWorklistId);
			
			String deleteItemUrl = site +"itemworklists/" + itemWorklistId + "/items/" + itemId;
			

			
			try {
				
				int deleteItemResult = UtilityTools.streamUrlDelete(deleteItemUrl, testSite);

				Assert.assertEquals(deleteItemResult, 204);
			
			} catch (Exception e) {
				success = false;
				System.err.println("unable to Delete an item worklist" + e.toString() + "\n");

			}
			/*
		// Verifying not found - 404 
			deleteItemUrl = site +"itemworklists/333/items/4";
			int deleteItemResult = UtilityTools.streamUrlDelete(deleteItemUrl, testSite);
			
			Assert.assertEquals(404, deleteItemResult);
			
		// Verifying not authorized - 403
			
			deleteItemUrl = site +"itemworklists/33/items/35";
			deleteItemResult = UtilityTools.streamUrlDelete(deleteItemUrl, testSite);
			Assert.assertEquals(403, deleteItemResult);
			*/
			return success;
			
		}
		
		private static int getItemWorklistId (String site, String id, String testSite) {
			String userUrl = site + "itemworklists?user="+id;
			int itemWorklistId = 0;
			try {
				String userWorklistsInfo = UtilityTools.streamUrl(userUrl, testSite);
				JSONArray userWorklists = new JSONArray(userWorklistsInfo);
				JSONObject lastWorklist = userWorklists.getJSONObject(userWorklists.length()-1);
				itemWorklistId = lastWorklist.getInt("id");
			} catch (Exception e) {
				success = false;
				System.out.println("Unable to get user info" + e.toString() + "\n");

			}
			
			return itemWorklistId;
		}
		
		private static int getItemId(String site, String testSite, int itemWorklistId) throws Exception{
			int itemId = 0;
			String myItemsUrl = site + "itemworklists/" + itemWorklistId;

			String itemWorklistItems = UtilityTools.streamUrl(myItemsUrl, testSite);
			JSONObject itemWorklistObj = new JSONObject(itemWorklistItems);
			JSONArray myWorklistItems = itemWorklistObj.getJSONArray("entries");
			int numOfItems = myWorklistItems.length();
			JSONObject WorklistItem = myWorklistItems.getJSONObject(numOfItems -1);
			itemId = WorklistItem.getInt("id");
			return itemId;
		}
		
		public static String getUserId (String site, String testSite, String site_id, String username) throws Exception {
			String myUserId = "";
			String myUserInfoUrl = site + "users?sitecode="+site_id+"&username="+username;

			String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, testSite);

			JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
			
			myUserId = myUserInfoObject.optString("id");
			
			return myUserId;
		}

	}

