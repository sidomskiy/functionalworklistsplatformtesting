package com.iii.tests;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import org.junit.Assert;
import org.junit.Test;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONObject;
import com.iii.common.*;
@SuppressWarnings("unused")

public class SendListItemsSierra {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {

			boolean success = true;
			long sleepTime = 60;
			JSONObject envInfo = UtilityTools.getEnvInfo(envName);
			 
			site = envInfo.getString("site");
			String oathsite = envInfo.getString("oathsite");
			String Thrifthost= envInfo.getString("Thrifthost");
			String username = envInfo.getString("username");
			String password = envInfo.getString("password");
			String libsite = envInfo.getString("libsite");
			
			
			
			int boolspecialId = CreateNewItemWorklist.test(site, envName);

			UtilityTools.MySleep(sleepTime);
			String urlString = site + "itemworklists/" + boolspecialId + "/export";
					
			// sending a list of items to Sierra as a boolspecial
			try {
				
				String sendListOfItemsToSierra=streamPost(urlString, envName);

				Assert.assertEquals("204", sendListOfItemsToSierra);

			} catch (Exception e) {
				success = false;
				System.out.println("\tUnable to sending a list of items to ILS: " + e + "\n");
				e.printStackTrace();

			}
		
			// Verifying invalid worklist - 400
				urlString = site + "itemworklists/333/export";
				String sendListOfItemsToSierra=streamPost(urlString, envName);
				Assert.assertEquals("400", sendListOfItemsToSierra.substring(0, 3));
			
			return success;
			

		}
		
		public static String streamPost (String urlString, String testSite) throws Exception {
			
			String inputStr = null;
			
			try {
				HttpClient client = new HttpClient();
				PostMethod postApiResp = new PostMethod(urlString);
				postApiResp.addRequestHeader("Authorization", ("Bearer " + UtilityTools.getAuthenticationToken(testSite)));
				int returnCode = client.executeMethod(postApiResp);
				if (returnCode == 204){
					inputStr=Integer.toString(returnCode);
				} else {
					BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
					inputStr=returnCode + " | "+ readAll(br);
				}
				
			} catch (Exception e) {
				System.out.println("\tunable to sending a list of items to ILS: " + e + "\n");
				e.printStackTrace();
				inputStr = e.toString();

			}
			
			
			return inputStr;
		}
		
		public static String readAll(Reader br) throws IOException {
		    StringBuilder sb = new StringBuilder();
		    int cp;
		    while ((cp = br.read()) != -1) {
		      sb.append((char) cp);
		    }
		    return sb.toString();
		  }

	}
