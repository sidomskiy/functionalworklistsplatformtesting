package com.iii.tests;


import org.json.JSONObject;
import org.json.JSONArray;
import org.junit.Assert;
import org.junit.Test;

import com.iii.common.UtilityTools;
@SuppressWarnings("unused")

public class Features {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String reqUrl = site + "features?sitecode="+ libsite;
		
		String myFeaturesInfo = UtilityTools.streamStringUrl(reqUrl, envName);
		
		JSONArray AvalFeatures = new JSONArray(myFeaturesInfo);
		for (int i=0; i< AvalFeatures.length(); i++){
			JSONObject oneFeature = AvalFeatures.getJSONObject(i);
			try {
				Assert.assertEquals(true, oneFeature.getBoolean("available"));
				Assert.assertNotNull(oneFeature.getString("feature"));
			} catch (Exception e) {
				success = false;
				e.printStackTrace();

			}
		}
		
		
		return success;
	}

}
