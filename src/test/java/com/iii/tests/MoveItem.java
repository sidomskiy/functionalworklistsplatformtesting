package com.iii.tests;

import org.json.JSONObject;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import com.iii.common.UtilityTools;
import com.iii.common.FindBibsBarcode;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
@SuppressWarnings("unused")

public class MoveItem {
	static int numLists = 0;
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String id = UtilityTools.getUserId(site, envName, libsite, username);
		String itemWorklistUrl = site + "itemworklists/moveEntries";  

		int fromList = createWorklist(site, envName, libsite, username);
		int itemInList = createItemToItemWorklist(site,fromList, envName);
		int toListId = createWorklist(site, envName, libsite, username);
		
		JSONObject newMovingItems = new JSONObject();
		newMovingItems.put("fromListId",fromList);
		newMovingItems.put("toListId", toListId);
		int[] entriesIds = {itemInList};
		newMovingItems.put("itemworklistEntriesIds", entriesIds);
		String post_json = newMovingItems.toString();
		
		try {
			String NewMovingItems =  streamUrlPost(itemWorklistUrl, post_json, envName);
			
		} catch (Exception e) {
			success = false;
			e.printStackTrace();

		}
		
		return success;
	}
	
	public static int createWorklist(String site, String testSite, String site_id, String username) throws Exception {
		numLists = numLists + 1;
		int worklitId = 0;
		String myUserId = "";
		String myUserInfoUrl = site + "users?sitecode="+site_id+"&username="+username;
		String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, testSite);
		JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
		
		myUserId = myUserInfoObject.optString("id");
		String itemWorklistName = "MovingItems test" + numLists;
		JSONObject newItmWkl = new JSONObject();
		newItmWkl.put("userId", myUserId);
		newItmWkl.put("name", itemWorklistName);
		String post_json = newItmWkl.toString();
		String newItemWorklist = site + "itemworklists/";		
				 
		JSONObject NewItmWkl =  UtilityTools.streamJObjectPost(newItemWorklist, post_json, testSite);
		
		return worklitId = NewItmWkl.getInt("id");
	}
	
	public static int createItemToItemWorklist(String site,int worklitId, String testSite) throws Exception{
		long sleepTime = 40;
		int itemId = 0;
		String newItemWklUrl = site + "itemworklists/" + worklitId + "/items";
		String barcode = FindBibsBarcode.singleBarcode(testSite);
		
		String json_string = "{\"barcode\":\""+ barcode +"\"}";
		UtilityTools.MySleep(sleepTime);
		JSONObject addItemEntryWorklist = UtilityTools.streamJObjectPost(newItemWklUrl, json_string, testSite);
		
		return itemId=addItemEntryWorklist.getInt("id");
	}
	
	public static String streamUrlPost (String urlString, String json_string, String testSite) throws Exception {
		String inputStr = null;
		try {
			StringRequestEntity requestEntity = new StringRequestEntity(json_string, "application/json", "UTF-8");
			HttpClient client = new HttpClient();
			PostMethod postApiResp = new PostMethod(urlString);
			postApiResp.addRequestHeader("Authorization", ("Bearer " + UtilityTools.getAuthenticationToken(testSite)));
			postApiResp.setRequestEntity(requestEntity);
			int returnCode = client.executeMethod(postApiResp);
			if (returnCode == 204){
				
				inputStr=Integer.toString(returnCode);
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
				inputStr=returnCode + " | "+ UtilityTools.readAll(br);
			}
		} catch (Exception e) {
			System.out.println("error from Utility tool: " + e.toString() + "\n");
			e.printStackTrace();
			inputStr = e.toString();

		}
		return inputStr;
	}

}
