package com.iii.tests;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONObject;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.junit.Assert;
import com.iii.common.JwtBuilder;

@SuppressWarnings("unused")
public class AppAuthTesting {
	
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		
		boolean success = true;

		
		String token = getAuthenticationToken(envName);
		
		//System.out.print("Token: Bearer " + token + "\n");
		JSONObject tokenInfo = new JSONObject(token);
		String bearerToken = tokenInfo.getString("access_token");
		
		
		try {
			Assert.assertTrue(bearerToken.length() > 5);
			Assert.assertEquals("Bearer", tokenInfo.getString("token_type"));
			Assert.assertEquals(599, tokenInfo.getInt("expires_in"));
			
		} catch (Exception e) {
			success = false;
			System.err.println("Error " + e.toString() + "\n");

		}
		
		
		
		return success;
		
	}
	
	public static String readAll(Reader br) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = br.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	}
	
	public static String getAuthenticationToken(String testSite) throws Exception{
		
		String platformInfo = "environments.xml";
		String site = "";
		String username = "";
		String password ="";
		String libsite = "";
		String token ="";
		try {
			 File file = new File(platformInfo);
			 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			 DocumentBuilder db = dbf.newDocumentBuilder();
			 Document document = db.parse(file);
			 document.getDocumentElement().normalize();
			 NodeList node = document.getElementsByTagName(testSite);
			 
			 Node nNode = node.item(0);
			 Element eElement = (Element) nNode;
			 
			 
			 site = getTagValue("oathsite", eElement);
			 username = getTagValue("username", eElement);
			 password = getTagValue("password", eElement);
			 libsite = getTagValue("libsite", eElement);
			 
		} catch (Exception e) {
			System.err.println("unable to open env info: " + e.toString());

		}
	
		String scope = "worklist";
		String tokenUrl = site + "oauth2/token";
		//String userinfo = "grant_type=password&username="+username+"&password="+password+"&site="+libsite+"&scope="+scope;
		String userinfo = "grant_type=client_credentials&scope="+scope+"&site="+libsite;
		String uuid = "1d8cf412-8fca-4b84-bf22-b93737a4bffe";
		String cid = "worklists";
		int expTimeInMinutes= 36000;

		String iosAppToken = JwtBuilder.generateToken(uuid, cid, expTimeInMinutes);
		//System.out.print("url: " + tokenUrl + "\n");
		//System.out.print("IIIJWT token: " + iosAppToken + "\n" + userinfo + "\n");
		try {
		String staffTokenInfo = postAuthUrl(tokenUrl, iosAppToken, userinfo);
			//System.out.println("Staff Token Info: "+ staffTokenInfo + "\n");
		
			token = staffTokenInfo;
		}catch (Exception e) {
			System.err.println("unable to create token: " + e.toString());

		}
		
		return token;
	}

	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
	 
	        Node nValue = (Node) nlList.item(0);
	 
		return nValue.getNodeValue();
	}
	
	public static String postAuthUrl (String urlString, String token, String userinfo) throws Exception {
		StringRequestEntity requestEntity = new StringRequestEntity(userinfo, "application/x-www-form-urlencoded", "UTF-8");
		HttpClient client = new HttpClient();
		PostMethod getApiResp = new PostMethod(urlString);
		getApiResp.addRequestHeader("Authorization", token);
		getApiResp.setRequestEntity(requestEntity);
		int returnCode = client.executeMethod(getApiResp);
		//System.out.print("return code: " + returnCode + "\n");
		BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		//System.out.println("Auth response: " + inputStr);
		return inputStr;
	}
		

}

