package com.iii.tests;

import org.json.JSONObject;
import org.json.JSONArray;
import org.junit.Assert;
import org.junit.Test;

import com.iii.common.UtilityTools;
import com.iii.resources.User;
@SuppressWarnings("unused")

public class GetUserInfoForSite {
	String envName = "qapl4";
	@Test
	public static boolean test(String site, String envName) throws Exception {
	//public void test() throws Exception {
		boolean success = true;
		
		JSONObject getEnvInfo = UtilityTools.getEnvInfo(envName);
		site = getEnvInfo.getString("site");
		String Thrifthost = getEnvInfo.getString("Thrifthost");
		String oathsite = getEnvInfo.getString("oathsite");
		String username = getEnvInfo.getString("username");
		String password = getEnvInfo.getString("password");
		String site_id = getEnvInfo.getString("libsite");
		
		String myUserInfoUrl = site + "users";
		
		String myUsersInfo = UtilityTools.streamStringUrl(myUserInfoUrl, envName);
		
		JSONArray myObject = new JSONArray(myUsersInfo);
		
		JSONObject myUserObject = myObject.getJSONObject(0) ;
		
		try {
			User MyUser = new User (myUserObject);
			Assert.assertNotNull(MyUser);
		} catch (Exception e) {
			success = false;
			System.err.println("Unable to creating User object " + e.toString() + "\n");

		}
		
		return success;
	}

}
