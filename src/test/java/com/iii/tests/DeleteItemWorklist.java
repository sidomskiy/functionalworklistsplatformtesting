package com.iii.tests;


import org.junit.Assert;
import org.junit.Test;
import org.json.JSONObject;
import com.iii.common.UtilityTools;
@SuppressWarnings("unused")

public class DeleteItemWorklist {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
			boolean success = true;
			
			JSONObject getEnvInfo = UtilityTools.getEnvInfo(envName);
			site = getEnvInfo.getString("site");
			String Thrifthost = getEnvInfo.getString("Thrifthost");
			String oathsite = getEnvInfo.getString("oathsite");
			String username = getEnvInfo.getString("username");
			String password = getEnvInfo.getString("password");
			String site_id = getEnvInfo.getString("libsite");
			
			String id = getUserId(site, envName, site_id, username);
			
			int worklistId = UtilityTools.getItemWorklistId(site, id, envName);
			String urlString = site + "itemworklists/" + worklistId;

			try {
				int deleteItemWorklistCode = UtilityTools.streamUrlDelete(urlString, envName);
				Assert.assertEquals(deleteItemWorklistCode, 202);
			} catch (Exception e) {
				success = false;
				System.out.println("\tunable to Delete an item worklist \n");
				e.printStackTrace();

			}

			return success;
		}
		public static String getUserId (String site, String testSite, String site_id, String username) throws Exception {
			String myUserId = "";
			String myUserInfoUrl = site + "users?sitecode="+site_id+"&username="+username;
			//System.out.print(myUserInfoUrl + " | " + testSite + "\n");
			String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, testSite);
			//System.out.print("User info" + myStringAllUsersJson + "\n");
			JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
			
			myUserId = myUserInfoObject.optString("id");
			
			return myUserId;
		}

	}