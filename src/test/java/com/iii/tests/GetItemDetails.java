package com.iii.tests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.iii.common.UtilityTools;
import java.util.Random;
@SuppressWarnings("unused")

public class GetItemDetails {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		int itemWorklistId = CreateNewItemWorklist.test(site, envName);
		
		String requestUrl = site + "items/"+ itemWorklistId;

		String itemId = getItemId(site, itemWorklistId, envName);
		String itemDetailsUrl = site + "items/"+ itemId;

		String itemDetails = UtilityTools.streamStringUrl(itemDetailsUrl, envName);

		JSONObject ItemDetails = new JSONObject(itemDetails);
		JSONObject ItemInfo = ItemDetails.getJSONObject("itemRecNum");
		
		
		try {
			Assert.assertEquals(itemId, ItemInfo.getString("value"));
			
		} catch (Exception e) {
			success = false;
			e.printStackTrace();

		}
		
		return success;
	}
	
	public static String getItemId(String site, int itemWorklistId, String envName) throws Exception{
		String itemId = null;
		String requestUrl = site + "itemworklists/"+ itemWorklistId;
		
		String itemWorklists = UtilityTools.streamStringUrl(requestUrl, envName);
		
		JSONObject JSONWorklists = new JSONObject(itemWorklists);
		JSONArray itemWorkList = JSONWorklists.getJSONArray("entries");
		// create random object 
        Random ran = new Random();
        int nxt = ran.nextInt(itemWorkList.length()); 
		JSONObject childItemObject = itemWorkList.getJSONObject(nxt);
		itemId = childItemObject.getString("recordNumber");
		itemId = itemId.substring(1);
		return itemId;
	}

}
