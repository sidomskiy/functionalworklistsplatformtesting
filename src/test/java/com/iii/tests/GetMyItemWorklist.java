package com.iii.tests;


import org.junit.Assert;
import org.junit.Test;
import org.json.JSONObject;
import org.json.JSONArray;
import com.iii.common.UtilityTools;
import com.iii.resources.User;
@SuppressWarnings("unused")

public class GetMyItemWorklist {
	
	@Test
	public static boolean test(String site, String testSite) throws Exception {

			boolean success = true;
			JSONObject getEnvInfo = UtilityTools.getEnvInfo(testSite);
			
			site = getEnvInfo.getString("site");
			String Thrifthost = getEnvInfo.getString("Thrifthost");
			String oathsite = getEnvInfo.getString("oathsite");
			String username = getEnvInfo.getString("username");
			String password = getEnvInfo.getString("password");
			String site_id = getEnvInfo.getString("libsite");
			
			String userId = getUserId(site, testSite, site_id, username);
			String platUrl = site + "itemworklists?user=" + userId;

			// Getting json string of user item worklists
			String myStringListsJson = UtilityTools.streamStringUrl(platUrl, testSite);

			JSONArray myArrayItemLists = new JSONArray(myStringListsJson);
			for (int i = 0; i < myArrayItemLists.length(); i++) {
				JSONObject childMyItemList = myArrayItemLists.getJSONObject(i);

			// Creating json User object
				JSONObject childMyItemListUser = childMyItemList.getJSONObject("user");
			// creating User object from JSONObject for verification json data
				try {
					User MyUser = new User(childMyItemListUser);
					Assert.assertNotNull(MyUser);

				} catch (Exception e) {
					success = false;
					System.out.println("\tunable to creating User object \n");
					e.printStackTrace();

				}
		
				
			}
			
			// Verifying user not found - 404
				
				String userInfoUrl = site + "itemworklists?user=88";
				String myStringUserInfo = UtilityTools.streamStringUrl(userInfoUrl, testSite);
				
				Assert.assertEquals("404", myStringUserInfo.substring(0, 3));
				
			// Verifying user not authorized - 403
			//	userInfoUrl = site + "itemworklists?user=1";
			//	myStringUserInfo = UtilityTools.streamStringUrl(userInfoUrl, testSite);
				
			//	Assert.assertEquals("403", myStringUserInfo.substring(0, 3));
				
			// Verifying user required - 400
				userInfoUrl = site + "itemworklists";
				myStringUserInfo = UtilityTools.streamStringUrl(userInfoUrl, testSite);
				
				Assert.assertEquals("400", myStringUserInfo.substring(0, 3));
			
			return success;
		}
		
		public static String getUserId (String site, String testSite, String site_id, String username) throws Exception {
			String myUserId = "";
			String myUserInfoUrl = site + "users?sitecode="+site_id+"&username="+username;

			String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, testSite);

			JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
			
			myUserId = myUserInfoObject.optString("id");
			
			return myUserId;
		}
		

	}
