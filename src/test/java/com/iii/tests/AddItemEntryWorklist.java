package com.iii.tests;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONObject;
import com.iii.common.FindBibsBarcode;
import com.iii.common.UtilityTools;
@SuppressWarnings("unused")

public class AddItemEntryWorklist {
	
	static int itemWorklistId = 0;
	static String user_id = ""; 
	static boolean success = true;
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		//public void test() throws Exception {
			
			JSONObject envInfo = UtilityTools.getEnvInfo(envName);

			site = envInfo.getString("site");
			String oathsite = envInfo.getString("oathsite");
			String Thrifthost= envInfo.getString("Thrifthost");
			String username = envInfo.getString("username");
			String password = envInfo.getString("password");
			String libsite = envInfo.getString("libsite");
			
			
			user_id = getUserId(site, envName, libsite, username);
			itemWorklistId = CreateNewItemWorklist.test(site, envName);
			//System.out.println("User id: " + user_id + "\n" + itemWorklistId + "\n" + envName + "\n");
			
			
			String urlString = site + "itemworklists/" + itemWorklistId + "/items";
			//System.out.println(urlString + "\n");
			
			String singBar = FindBibsBarcode.singleBarcode(envName);
			
		// Creating a addItemWorklist json
			JSONObject obj=new JSONObject();
		// add barcode
			obj.put("barcode", singBar);  // 32156213500012   32200000048106
			
		// Creating json string
			 
			String json_string = obj.toString();
		  //System.out.println(urlString + "\n");
		  //System.out.println(json_string + "\n");
		        
		// Adding an item-entry to a worklist
		     
			int addItemEntryWorklist = streamUrlPost(urlString, json_string, envName);
			
			//System.out.println("result: " + addItemEntryWorklist + "\n");
			
			
			if (addItemEntryWorklist== 200) {
				success = true;
		    }else{
		    	success = false;
		    }	

		   return success;
			
		}
		

		public static String getUserId (String site, String testSite, String site_id, String username) throws Exception {
			String myUserId = "";
			String myUserInfoUrl = site + "users?sitecode="+site_id+"&username="+username;
			//System.out.print(myUserInfoUrl + " | " + testSite + "\n");
			String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, testSite);
			//System.out.print("User info" + myStringAllUsersJson + "\n");
			JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
			
			myUserId = myUserInfoObject.optString("id");
			
			return myUserId;
		}
		
		public static int streamUrlPost (String urlString, String json_string, String testSite) throws Exception {
			int returnCode = 0;
			try {
				StringRequestEntity requestEntity = new StringRequestEntity(json_string, "application/json", "UTF-8");
				HttpClient client = new HttpClient();
				PostMethod postApiResp = new PostMethod(urlString);
				
				postApiResp.addRequestHeader("Authorization", ("Bearer " + UtilityTools.getAuthenticationToken(testSite)));
				postApiResp.setRequestEntity(requestEntity);

				returnCode = client.executeMethod(postApiResp);
				
			} catch (Exception e) {
				
				e.printStackTrace();

			}
			return returnCode;
		}

	}
