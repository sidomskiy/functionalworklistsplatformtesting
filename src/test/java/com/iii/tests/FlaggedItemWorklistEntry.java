package com.iii.tests;


import org.json.JSONObject;

import java.util.Random;

import org.json.JSONArray;
import com.iii.common.FindBibsBarcode;
import com.iii.common.UtilityTools;
import org.junit.Assert;
import org.junit.Test;
@SuppressWarnings("unused")

public class FlaggedItemWorklistEntry {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		long sleepTime = 40;
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String id = UtilityTools.getUserId(site, envName, libsite, username);
		int worklitId = CreateNewItemWorklist.test(site, envName);
		int itemId = getItemBarcode(worklitId, envName);
		
		String reqUrl = site + "itemworklists/" + worklitId + "/items/" + itemId;
		
		boolean frag = getItemFlaggedStatus(site, worklitId, envName);
		
		if (frag == true){
			frag=false;
		}else {
			frag = true;
		}
		JSONObject newFlag = new JSONObject();
		newFlag.put("flag", frag);
		String flag_json = newFlag.toString();
		
		UtilityTools.MySleep(sleepTime);
		String result = UtilityTools.streamUrlPut(reqUrl, flag_json, envName);
		
		
		JSONObject itemInfo = new JSONObject(result);
		try {
			Assert.assertEquals(frag, itemInfo.getBoolean("flagged"));
		} catch (Exception e) {
			success = false;
			System.err.println (e.toString() + "\n");

		}
		
		
		return success;
	}
	
	public static int getItemBarcode(int itemWorklistId, String envName) throws Exception{
		int itemBarcode = 0;
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		String site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String requestUrl = site + "itemworklists/"+ itemWorklistId;
		
		String itemWorklist = UtilityTools.streamStringUrl(requestUrl, envName);
		
		JSONObject JSONWorklists = new JSONObject(itemWorklist);
		JSONArray itemWorkList = JSONWorklists.getJSONArray("entries");
		// create random object 
        Random ran = new Random();
        int nxt = ran.nextInt(itemWorkList.length()); 
		JSONObject childItemObject = itemWorkList.getJSONObject(nxt);
		
		itemBarcode = childItemObject.getInt("id");
		return itemBarcode;
	}
	
	public static boolean getItemFlaggedStatus(String site, int worklitId, String testSite) throws Exception {
		boolean flaggesStatus = false;
		String requestUrl = site + "itemworklists/"+ worklitId;
		
		String itemWorklists = UtilityTools.streamStringUrl(requestUrl, testSite);
		JSONObject myUserInfoObject =  new JSONObject(itemWorklists);
		JSONArray itemEtries = myUserInfoObject.getJSONArray("entries");
		JSONObject itemEntrie = itemEtries.getJSONObject(0);
		boolean flagged = itemEntrie.getBoolean("flagged");
		return flaggesStatus= flagged;
	}

}
