package com.iii.tests;

import org.json.JSONObject;
import com.iii.common.UtilityTools;
import org.junit.Assert;
import org.junit.Test;
@SuppressWarnings("unused")

public class GetLibrarieById {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		int siteId = getSiteId(site, envName, libsite);
		String getLibrarieUrl = site + "sites/" + siteId;
		String response = UtilityTools.streamStringUrl(getLibrarieUrl, envName);
		JSONObject JSONWorklists = new JSONObject(response);
		
		try {
			Assert.assertEquals(libsite, JSONWorklists.getString("siteCode"));
			
		} catch (Exception e) {
			success = false;
			e.printStackTrace();

		}
		return success;
	}
	
	public static int getSiteId (String site, String envName, String site_id) throws Exception{
		int siteId = 0;
		String mySiteInfoUrl = site + "sites?sitecode=" + site_id;
		
		String mySiteInfo = UtilityTools.streamStringUrl(mySiteInfoUrl, envName);
		
		JSONObject mySiteObject = new JSONObject(mySiteInfo);
		siteId = mySiteObject.getInt("id");
		return siteId;
	}

}
