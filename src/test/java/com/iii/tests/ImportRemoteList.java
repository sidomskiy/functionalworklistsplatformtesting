package com.iii.tests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.Random;
import com.iii.common.UtilityTools;
@SuppressWarnings("unused")

public class ImportRemoteList {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		 site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String newItemWorklist = site + "itemworklists";
		
		String id = UtilityTools.getUserId(site, envName, libsite, username);
		
		int itemReviewId = getRendRemListId(site, envName);
			
		String itemWorklistName = "importedautolist_"+ UtilityTools.getTimeStamp();
		JSONObject newItmWkl = new JSONObject();
		newItmWkl.put("userId", id);
		newItmWkl.put("name", itemWorklistName);
		newItmWkl.put("remoteListId", itemReviewId);
		String post_json = newItmWkl.toString();
			
		String myContentsItemReviewFile = UtilityTools.streamUrlPost(newItemWorklist, post_json, envName);
		
		JSONObject remItemWorklist = new JSONObject(myContentsItemReviewFile);
		if(remItemWorklist.getInt("id") < 1){
			success = false;
		}
		
		return success;
	}
	
	public static int getRendRemListId(String site, String envName) throws Exception{
		String reqUrl = site + "remoteitemlists";
		String response = UtilityTools.streamStringUrl(reqUrl, envName);
		JSONArray remList = new JSONArray (response);
		Random rand = new Random(); 
		int  n = rand.nextInt(remList.length());
		JSONObject remListObject = remList.getJSONObject(n);
		int randId = remListObject.getInt("id");
		return randId;
	}

}
