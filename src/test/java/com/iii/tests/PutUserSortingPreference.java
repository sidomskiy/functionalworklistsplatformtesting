package com.iii.tests;


import org.json.JSONArray;
import org.json.JSONObject;
import com.iii.common.UtilityTools;
import org.junit.Assert;
import org.junit.Test;
@SuppressWarnings("unused")

public class PutUserSortingPreference {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		 site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String myUserInfoUrl = site + "users?sitecode=" + libsite + "&username=" + username;
		String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, envName);
		JSONObject UserInfo = new JSONObject(myStringAllUsersJson);
		int id = UserInfo.getInt("id");
		JSONObject ListSortOrder = UserInfo.getJSONObject("listSortOrder");
		
		
		String newSortDicrection;
		String newSortType;
		
		if (ListSortOrder.getString("listSortDirection").equals("desc")){
			newSortDicrection = "asc";
		} else {
			newSortDicrection = "desc";
		}
		
		if (ListSortOrder.getString("listSortType").equals("lastUpdated")){
			newSortType = "name";
		}else {
			newSortType = "lastUpdated";
		}
		
		JSONObject newSortOrder = new JSONObject();
		newSortOrder.put("listSortType", newSortType);
		newSortOrder.put("listSortDirection", newSortDicrection);
		
		String put_json = newSortOrder.toString();
		String urlString = site + "users/" + id;
	
		String putinfo = UtilityTools.streamUrlPut(urlString, put_json, envName);
		JSONObject RespUserInfo = new JSONObject(putinfo);
		JSONObject PutlistSortOrder = RespUserInfo.getJSONObject("listSortOrder");
		
		try {
			Assert.assertEquals(newSortDicrection, PutlistSortOrder.getString( "listSortDirection"));
			Assert.assertEquals(newSortType, PutlistSortOrder.getString("listSortType"));
		}catch (Exception e) {
			success = false;
			System.err.println("Unable to creating User object " + e.toString() + "\n");

		}
	
		
		return success;
	}

}
