package com.iii.tests;


import org.json.JSONObject;
import org.junit.Test;

import com.iii.common.UtilityTools;
import org.json.JSONArray;
@SuppressWarnings("unused")

public class GetRemoteItemLists {
	
	@Test
	public static boolean test(String site, String envName) throws Exception {
		boolean success = true;
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		 
		 site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String reqUrl = site + "remoteitemlists";
		
		String response = UtilityTools.streamStringUrl(reqUrl, envName);
		
		JSONArray remList = new JSONArray (response);
		
		if(remList.length() < 1){
			success = false;
		}
		
		return success;
	}

}
