package com.iii.tests;


import org.json.JSONObject;
import org.json.JSONArray;
import com.iii.common.UtilityTools;
import com.iii.resources.User;
import com.iii.resources.Item;
import org.junit.Assert;
import org.junit.Test;
@SuppressWarnings("unused")

public class GetItemWorklist {
	
	static String platUrl = null;
	static int worklistId = 0;
	
	@Test
	public static boolean test(String site, String testSite) throws Exception {
		JSONObject getEnvInfo = UtilityTools.getEnvInfo(testSite);
		site = getEnvInfo.getString("site");
		String Thrifthost = getEnvInfo.getString("Thrifthost");
		String oathsite = getEnvInfo.getString("oathsite");
		String username = getEnvInfo.getString("username");
		String password = getEnvInfo.getString("password");
		String site_id = getEnvInfo.getString("libsite");
		
		String userId = getUserId(site, testSite, site_id, username);

		//getItemWorklistId
		worklistId = UtilityTools.getItemWorklistId(site, userId, testSite);
		boolean success = true;
		platUrl = site + "itemworklists/" + worklistId;
		
	// Getting json String of user item worklists
		
		String itemWorklists = UtilityTools.streamStringUrl(platUrl, testSite);
		
	// creating json object from json string
		JSONObject JSONWorklists = new JSONObject(itemWorklists);
		
		int workId = JSONWorklists.getInt("id");

	// verification of the worklist id
		Assert.assertEquals(worklistId, workId);
		
	// creating json user object
		JSONObject WorkListUser =  JSONWorklists.getJSONObject("user");
		
	// creating User object from JSONObject for verification json data
		User MyUserW = new User(WorkListUser);
		Assert.assertNotNull(MyUserW);
		
	
	//creating json array of the items
		
		JSONArray itemWorkList = JSONWorklists.getJSONArray("entries");

		
	// creating User object from JSONObject for verification json data
		
		for (int i=0; i < itemWorkList.length(); i++){
			
			JSONObject childItemObject = itemWorkList.getJSONObject(i);
			try {
				Item MyWorkListItem = new Item(childItemObject);
				Assert.assertNotNull(MyWorkListItem);
				
			} catch (Exception e) {
				success = false;
				System.out.println("unable to creating Item object" + e.toString() + "\n");
			

			}
			
			
		}
	
		return success;
	}
	
	public static String getUserId (String site, String testSite, String site_id, String username) throws Exception {
		String myUserId = "";
		String myUserInfoUrl = site + "users?sitecode="+site_id+"&username="+username;
		
		String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, testSite);
	
		JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
		
		myUserId = myUserInfoObject.optString("id");
		
		return myUserId;
	}
	

}

