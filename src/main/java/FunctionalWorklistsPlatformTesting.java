
/**

 * @version $Id: FunctionalWorklistsPlatformTesting.java 247939 2015-06-01 17:30:14Z sergeii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import junit.framework.Assert;
import com.iii.common.*;
import com.iii.resources.*;
import com.iii.tests.*;


@SuppressWarnings("unused")
public class FunctionalWorklistsPlatformTesting {
	
	static String envName ="qapl4"; // qa - qa platform with devops-7490-app.iii-lab.eu
									// qapl3  - qa platform with qa-sierra-app03.iii.com
									// qapl4  - qa platform with qa-sierra-app04.iii.com
									// staging - staging platform with qa-sierra-app04.iii.com
									// stagingeu - EU staging platform with qa-sierra-app04.iii.com
									// stagingap - AP staging platform with qa-sierra-app04.iii.com
									// prod3  - production platform with qa-sierra-app03.iii.com
									// prod4  - production platform with qa-sierra-app04.iii.com
									// prodeu  - EU production platform with qa-sierra-app04.iii.com
									// prodau - AP production platform with qa-sierra-app04.iii.com
	boolean createReport; 
	static String nameFile = "testResult.txt";
	
	
	@Test
	public void test() throws Exception {
		long sleepTime = 40;
		System.setProperty("jsse.enableSNIExtension", "false");
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		
		
		String site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		deleteInfoFile(nameFile);
		

		try {
			FileOutputStream writer = new FileOutputStream(nameFile);
			writer.close();
		} catch (FileNotFoundException e1) {
			System.out.println(e1.toString()+"\n");
			e1.printStackTrace();
		}
		
		System.out.print(envName + "\n" + site + "\n");
		
		createReport = UtilityTools.writeStrToFile((getDateTime()+" | Start Platform Infrastructure Testing | START\n"), nameFile);
		

		
		// Authentication Testing
		
		boolean authTesting = AuthenticationTesting.test(site, envName);
		if (authTesting){
					
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Authentication Testing | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Authentication Testing | Failed\n"), nameFile);
					
		}
				
		boolean authAppTesting = AppAuthTesting.test(site, envName);
		if (authAppTesting){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Authentication App Testing | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Authentication App Testing | Failed\n"), nameFile);
			
		}
		
	// 1.	Get user info by id
		
		boolean getUserIdVerifyInfo = GetUserInfoById.test(site, envName);
		if (getUserIdVerifyInfo){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Retrieving User Information by Id | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Retrieving User Information by Id | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// 2.	Get user info by username and site
		
		boolean GetUserInfoByNameSiteInfo = GetUserInfoByNameSite.test(site, envName);
		if (GetUserInfoByNameSiteInfo){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Retrieving User Information by by username and site | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Retrieving User Information by by username and site | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// Get user info by site
		
		boolean getUserInfoBySite = GetUserInfoForSite.test(site, envName);
		if(getUserInfoBySite){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Retrieving User Information by by site | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Retrieving User Information by by site | Failed\n"), nameFile);
			
		}
		
	// Put user's sorting preferences
		boolean putUserSortingPref = PutUserSortingPreference.test(site, envName);
		if(putUserSortingPref){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Put user's sorting preferences | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Put user's sorting preferences | Failed\n"), nameFile);
			
		}
		
		
	// 3.	Get an item worklist
		int newId = CreateNewItemWorklist.test(site, envName);
		UtilityTools.MySleep(sleepTime);
		boolean getItemWorlist = GetItemWorklist.test(site, envName);
		
		if (getItemWorlist){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Retrieving an item worklist | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Retrieving an item worklist | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// 4.	Get item worklists for a user
		
		int newWId = CreateNewItemWorklist.test(site, envName);
		UtilityTools.MySleep(sleepTime);
		boolean getUserItemWorklist = GetMyItemWorklist.test(site, envName);
		
		if (getUserItemWorklist){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Retrieving User Item Worklist Information | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Retrieving User Item Worklist Information | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// 5.	Create an item worklist
		
		UtilityTools.MySleep(sleepTime);
		int worklistId = CreateNewItemWorklist.test(site, envName);
		if (worklistId > 0){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Create an item worklist | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Create an item worklist | Failed\n"), nameFile);
			
		}
		
		UtilityTools.MySleep(sleepTime);
		
	// import remote list
		boolean importRemoteList = ImportRemoteList.test(site, envName);
		if(importRemoteList){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing import remote list | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing import remote list | Failed\n"), nameFile);
			
		}
		
	// 6.	Delete an item worklist
		
		UtilityTools.MySleep(sleepTime);
		boolean deleteItemWorklist = DeleteItemWorklist.test(site, envName);
		if (deleteItemWorklist){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Delete an item worklist | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Delete an item worklist | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// Get remote item lists
		boolean getRemoteItemLists = GetRemoteItemLists.test(site, envName);
		if(getRemoteItemLists){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Get remote item lists | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Get remote item lists | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// 7.	Export item worklist to ILS
		
		UtilityTools.MySleep(sleepTime);
		boolean exportItemWorklist = SendListItemsSierra.test(site, envName);
		
		if (exportItemWorklist){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Export item worklist to ILS | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Export item worklist to ILS | Failed\n"), nameFile);
			
		}
		
		UtilityTools.MySleep(sleepTime);
		
	// 8.	Add an item to an item worklist
		
		boolean addItemToItemWorklist = AddItemEntryWorklist.test(site, envName);
		UtilityTools.MySleep(sleepTime);
		if (addItemToItemWorklist){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Add an item to an item worklist | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Add an item to an item worklist | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		/*
	// Move items(ItemWorklistEntries) from one item worklist to another
		UtilityTools.MySleep(sleepTime);
		boolean moveItem = MoveItem.test(site, envName);
		if(moveItem){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Move items(ItemWorklistEntries) from one item worklist to another | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Move items(ItemWorklistEntries) from one item worklist to another | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		*/
		
	// 9.	Remove an item from an item worklist
		
		int mynewid = CreateNewItemWorklist.test(site, envName);
		System.out.println(mynewid);
		UtilityTools.MySleep(sleepTime);
		boolean deleteItemFromItemWorklist = RemoveItemFromItemWorklist.test(site, envName);
		
		if (deleteItemFromItemWorklist){
			
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Remove an item from an item worklist | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Remove an item from an item worklist | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// Modify item worklist entry - flagged
		boolean flaggedItemWorklistEntry = FlaggedItemWorklistEntry.test(site, envName);
		UtilityTools.MySleep(sleepTime);
		if(flaggedItemWorklistEntry){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Modify item worklist entry | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Modify item worklist entry | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
	
	// Get item details
		boolean getItemDetails = GetItemDetails.test(site, envName);
		if(getItemDetails){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Get item details  | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Get item details  | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// Get Item Summary
		boolean getItemSummary = GetItemSummary.test(site, envName);
		if(getItemSummary){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Get Item Summary  | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Get Item Summary  | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// Get details for a worklist library by id
		
		boolean getLibrarieById = GetLibrarieById.test(site, envName);
		if(getLibrarieById){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Get details for a worklist library by id  | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Get details for a worklist library by id | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// Get details for a worklist library by sitecode
		boolean getLibrarieBySitecode = GetLibrarieBySitecode.test(site, envName);
		if(getLibrarieBySitecode){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Get details for a worklist library by sitecode | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Get details for a worklist library by sitecode | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// Checkin an item
		
		
		
	// Create - Delete Bookmark
		boolean reateDeleteBookmark = CreateDeleteBookmark.test(site, envName);
		if(reateDeleteBookmark){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Create - Delete Bookmark | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Create - Delete Bookmark | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
	// Test Features Sierra Supports
		boolean features = Features.test(site, envName);
		if(features){
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Pass Testing Features Sierra Support | PASS\n"), nameFile);
		} else {
			createReport = UtilityTools.writeStrToFile((getDateTime()+" | Failed Testing Features Sierra Support | Failed\n"), nameFile);
			
		}
		UtilityTools.MySleep(sleepTime);
		
		
	}
	
	
	public static String getDateTime() {
		String dateFormatNow = "MM-dd-yyyy_HH-mm-ss";
		String dateTimeStr = null;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormatNow);
		dateTimeStr = sdf.format(cal.getTime());
		return dateTimeStr;
	}
	
	public static void deleteInfoFile (String fileName) {
		File fileTemp = new File(fileName);
		if (fileTemp.exists()){
			fileTemp.delete();
		}
	}

}
