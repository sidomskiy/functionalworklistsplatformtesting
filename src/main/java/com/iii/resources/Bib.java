package com.iii.resources;

import java.lang.reflect.Array;

import org.json.JSONArray;
import org.json.JSONObject;

public class Bib {
	
	private String id;
	private boolean available;
	private String author;
	private JSONObject materialType;
	private int publishYear;
	private String isbn;
	private String pubInfo;
	private String description;
	private boolean requestable;
	private String edition;
	private String series;
	private int holdsCount;
	private int itemCount;
	private JSONArray externalLinks;
	private String eVendor;
	private String eVendorItemId;
	
	
	public String getId() { return id; }
	public boolean getAvailable() {return available; }
	public String getAuthor() { return author; }
	public JSONObject getMaterialType() { return materialType; }
	public int getPublishYear() { return publishYear;}
	public String getIsbn() { return isbn; }
	public String getPubInfo() { return pubInfo; }
	public String getDescription() { return description; }
	public boolean getRequestable() { return requestable; }
	public String getEdition() { return edition; }
	public String getSeries() { return series; }
	public int getHoldCount() { return holdsCount; }
	public int getItemCount() { return itemCount; }
	public JSONArray getExternalLinks() { return externalLinks; }
	public String getEVendor() { return eVendor; }
	public String getEVendorItemId() { return eVendorItemId; }
	
	
	public void setId(String id) { this.id = id; }
	public void setAvailable(boolean available) {this.available = available; }
	public void setAuthor(String author) { this.author = author; }
	public void setMaterialType(JSONObject materialType) { this.materialType = materialType; }
	public void setPublishYear(int publishYear) { this.publishYear = publishYear;}
	public void setIsbn(String isbn) { this.isbn = isbn; }
	public void setPubInfo(String pubInfo) { this.pubInfo = pubInfo; }
	public void setDescription(String description) { this.description = description; }
	public void setRequestable(boolean requestable) { this.requestable = requestable; }
	public void setEdition(String edition) { this.edition = edition; }
	public void setSeries(String series) { this.series = series; }
	public void setHoldCount(int holdsCount) { this.holdsCount = holdsCount; }
	public void setItemCount(int itemCount) { this.itemCount = itemCount; }
	public void setExternalLinks(JSONArray jsonArray) { this.externalLinks = jsonArray; }
	public void setEVendor(String eVendor) { this.eVendor = eVendor; }
	public void setEVendorItemId(String eVendorItemId) { this.eVendorItemId = eVendorItemId; }
	
	
	
	public Bib() {
		
	}
	
   public Bib(JSONObject childBib) {
	   
	   setId(childBib.getString("id"));
	   setAvailable(childBib.optBoolean("available"));
	   setAuthor(childBib.getString("author"));
	   setMaterialType(childBib.getJSONObject("materialType"));
	   setPublishYear(childBib.optInt("publishYear"));
	   setIsbn(childBib.optString("isbn"));
	   setPubInfo(childBib.optString("pubInfo"));
	   setDescription(childBib.optString("description"));
	   setRequestable(childBib.optBoolean("requestable"));
	   setEdition(childBib.optString("edition"));
	   setSeries(childBib.optString("series"));
	   setHoldCount(childBib.getInt("holdsCount"));
	   setItemCount(childBib.getInt("itemCount"));
	   setExternalLinks(childBib.optJSONArray("externalLinks"));
	   setEVendor(childBib.optString("eVendor"));
	   setEVendorItemId(childBib.optString("eVendorItemId"));
	   
		
	}
	
	

}
