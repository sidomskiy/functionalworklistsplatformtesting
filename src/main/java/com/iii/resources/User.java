package com.iii.resources;

import org.json.JSONObject;

/**

 * @version $Id: User.java 245983 2015-04-06 22:43:57Z iii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */

public class User {
	
	private int id;
	private String username;
	private String password;
	private String displayName;
	private int siteId;
	
	public int getId() { return id; }
	public String getUsername() { return username; }
	public String getPassword() { return password; }
	public String getDisplayName() { return displayName; }
	public int getSiteId() { return siteId; }
	
	
	public void setId(int id) { this.id = id; }
	public void setUsername(String username) { this.username = username; }
	public void setPassword(String password) { this.password = password; }
	public void setDisplayName(String displayName) { this.displayName = displayName; }
	public void setSiteId(int siteId) { this.siteId = siteId; }
	
	public User(){
		
	}
	
    public User(JSONObject childUserObject){
    	setId(childUserObject.getInt("id"));
    	setUsername(childUserObject.getString("username"));
    	setPassword(childUserObject.optString("password"));
    	setDisplayName(childUserObject.optString("displayName"));
    	setSiteId(childUserObject.optInt("siteId"));    //.getInt("siteId"));
    	
		
	}

}
