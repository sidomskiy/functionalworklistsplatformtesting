package com.iii.resources;

import org.json.JSONObject;

public class MaterialTypeCategories {
	
	private String materialTypeCategory;
	private String secondaryMaterialTypeCategory;
	
	public String getmaterialTypeCategory() { return materialTypeCategory; }
	public String getsecondaryMaterialTypeCategory() { return secondaryMaterialTypeCategory; }
	
	public void setmaterialTypeCategory(String materialTypeCategory) { this.materialTypeCategory = materialTypeCategory; }
	public void setsecondaryMaterialTypeCategory(String secondaryMaterialTypeCategory) { this.secondaryMaterialTypeCategory = secondaryMaterialTypeCategory; }
	
	public MaterialTypeCategories(){
		
	}

	public MaterialTypeCategories(JSONObject maTypeCat){
		setmaterialTypeCategory(maTypeCat.getString("materialTypeCategory"));
		setsecondaryMaterialTypeCategory(maTypeCat.getString("secondaryMaterialTypeCategory"));
		
	}

}
