package com.iii.resources;
/**

 * @version $Id: MyTrustManager.java 245983 2015-04-06 22:43:57Z iii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */
import java.security.cert.*;
import javax.net.ssl.X509TrustManager;

public class MyTrustManager implements X509TrustManager {
	
	public void checkClientTrusted(X509Certificate[] chain, String authType) {
	}

	public void checkServerTrusted(X509Certificate[] chain, String authType) {
	}

	public X509Certificate[] getAcceptedIssuers() {
		return new X509Certificate[0];
	}
	

}
