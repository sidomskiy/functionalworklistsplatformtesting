package com.iii.resources;

import com.iii.tests.CreateNewItemWorklist;

public class ItemWorklist {
	public ItemWorklist() {
    }
	
	public void doWork() throws Exception {
        try {
        	//TestCreateNewItemWorklist.test();
        	System.out.println("Start Work"  + new java.util.Date());
        	Thread.sleep(2000);
        } catch (InterruptedException e) {
        	System.out.println(e.toString());
        }
    }
	
	public static void main(String[] args) throws Exception {
		ItemWorklist worker = new ItemWorklist();
        System.out.println("Start Work"  + new java.util.Date());
        worker.doWork();
        System.out.println("... try to do something while the work is being done....");
 
        System.out.println("End work" + new java.util.Date());
        System.exit(0);
    }

}
