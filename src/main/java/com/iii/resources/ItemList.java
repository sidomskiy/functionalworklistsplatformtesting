package com.iii.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONObject;

import org.junit.Test;
import com.iii.common.UtilityTools;
import com.iii.resources.ItemList;
import com.google.gson.Gson;

/**

 * @version $Id: ItemList.java 245983 2015-04-06 22:43:57Z iii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */

public class ItemList {
	
	private int id;
	private String name;
	private User MyUser = new User();
	private List<Item> MyItem = new ArrayList<Item>();
	
	
	public ItemList(){
		
	}
	
	public ItemList(JSONArray jsonMyListArray){
		
		for (int i = 0; i < jsonMyListArray.length(); i++) {
			
			JSONObject childJSONObject = jsonMyListArray.getJSONObject(i);
			
			setId(childJSONObject.getInt("id"));
			setName(childJSONObject.getString("name"));
			
			JSONObject userInfo = childJSONObject.getJSONObject("user");
			Gson gson = new Gson();
			this.MyUser = gson.fromJson(userInfo.toString(), User.class);
			org.json.JSONArray listItemArray = (org.json.JSONArray) childJSONObject.getJSONArray("entries");
			if (listItemArray.length() > 1){
				System.out.println(listItemArray.length() + "\n");
				for (int j =0; j<listItemArray.length(); j++){
					
					JSONObject itemListchild = listItemArray.getJSONObject(j);
					MyItem.add(gson.fromJson(itemListchild.toString(), Item.class));
					//this.MyItem = (List<Item>) gson.fromJson(itemListchild.toString(), Item.class);
					
					
				}
			}
			
		     
		    
		}
		
	}
	
	public int getId() { return id; }
	public String getName() { return name; }
	public User getMyUser() { return MyUser; }
	public List<Item> getMyItem() { return MyItem; }
	
	
	public void setId(int id) { this.id = id; }
	public void setName(String name) { this.name = name; }
	public void setMyUser(User MyUser) { this.MyUser = MyUser; }
	public void setMyItem(List<Item> MyItem) { this.MyItem = MyItem; }
	

}
