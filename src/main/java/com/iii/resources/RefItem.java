package com.iii.resources;
/**

 * @version $Id: RefItem.java 245983 2015-04-06 22:43:57Z iii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */
import org.json.JSONObject;


public class RefItem {
	
	private int id;
	private String recordNumber;
	private String lastUpdated;
	
	public int getId() { return id; }
	public String getRecordNumber() { return recordNumber; }
	public String getLastUpdated() { return lastUpdated; }
	
	
	public void setId(int id) { this.id = id; }
	public void setRecordNumber(String recordNumber) { this.recordNumber = recordNumber; }
	public void setLastUpdated(String lastUpdated) { this.lastUpdated = lastUpdated; }
	
	
	public RefItem(){
		
	}
	
	public RefItem(JSONObject childItemObject){
    	setId(childItemObject.getInt("id"));
    	setRecordNumber(childItemObject.getString("recordNumber"));
    	setLastUpdated(childItemObject.getString("lastUpdated"));
	}
	

}
