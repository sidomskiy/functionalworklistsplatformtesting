package com.iii.resources;

/**

 * @version $Id: Item.java 245983 2015-04-06 22:43:57Z iii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */
import org.json.JSONObject;



public class Item {
	
	private int id;
	private String barcode;
	private String itemTypeName;
	private String publicationInfo;
	private String recordNumber;
	private String author;
	private String location;
	private String title;
	private int worklistId;
	private String imageUrl;
	private String volumeInfo=null;
	private String callNumber;
	private String publicationDate;
	private String isbnUpc;
	
	
	public int getId() { return id; }
	public String getBarcode() { return barcode; }
	public String getItemTypeName() { return itemTypeName; }
	public String getPublicationInfo() { return publicationInfo; }
	public String getRecordNumber() { return recordNumber; }
	public String getAuthor() { return author; }
	public String getLocation() { return location; }
	public String getTitle() { return title; }
	public int getWorklistId() { return worklistId; }
	public String getImageUrl() { return imageUrl; }
	public String getVolumeInfo() { return volumeInfo; }
	public String getCallNumber() { return callNumber; }
	public String getPublicationDate() { return publicationDate; }
	public String getIsbnUpc() { return isbnUpc; }
	
	
	
	public void setId(int id) { this.id = id; }
	public void setBarcode(String barcode) { this.barcode = barcode; }
	public void setItemTypeName(String itemTypeName) { this.itemTypeName = itemTypeName; }
	public void setPublicationInfo(String publicationInfo) { this.publicationInfo = publicationInfo; }
	public void setRecordNumber(String recordNumber) { this.recordNumber = recordNumber; }
	public void setAuthor(String author) { this.author = author; }
	public void setLocation(String location) { this.location = location; }
	public void setTitle(String title) { this.title = title; }
	public void setWorklistId(int worklistId) { this.worklistId = worklistId; }
	public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }
	public void setVolumeInfo(String volumeInfo) { this.volumeInfo = volumeInfo; }
	public void setCallNumber(String callNumber) { this.callNumber = callNumber; }
	public void setPublicationDate(String publicationDate) { this.publicationDate = publicationDate; }
	public void setIsbnUpc(String isbnUpc) { this.isbnUpc = isbnUpc; }
	
	
	public Item(){
		
	}
	
    public Item(JSONObject childItemObject){
    	setId(childItemObject.optInt("id"));
    	setBarcode(childItemObject.getString("barcode"));
    	setItemTypeName(childItemObject.optString("itemTypeName", ""));
    	setPublicationInfo(childItemObject.optString("publicationInfo", ""));
    	setRecordNumber(childItemObject.optString("recordNumber", ""));
    	setAuthor(childItemObject.optString("author", ""));
    	setLocation(childItemObject.optString("location", ""));
    	setTitle(childItemObject.optString("title", ""));
    	setWorklistId(childItemObject.optInt("worklistId"));
    	setImageUrl(childItemObject.optString("imageUrl"));
    	setVolumeInfo(childItemObject.optString("volumeInfo"));
    	setCallNumber(childItemObject.optString("callNumber"));
    	setPublicationDate(childItemObject.optString("publicationDate"));
    	setIsbnUpc(childItemObject.optString("isbnUpc"));
    	
    }
	

}
