package com.iii.resources;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import com.iii.common.JwtBuilder;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.parsers.ParserConfigurationException;
import com.iii.common.*;
import org.json.JSONObject;
import org.xml.sax.SAXException;

public class Token {
	
	private static String connectionURL="jdbc:postgresql://localhost:5432/Automation";
	private static String driverURL="org.postgresql.Driver";
	private static String dbuser="postgres";
	private static String dbpassword="asdasd1";
	
	private String access_token;
	private String token_type;
	private Date expires_in;
	private String refresh_token;
	
	
	public String getaccess_token(){ return this.access_token; }
	public String gettoken_type(){ return this.token_type; }
	public Date getexpires_in(){ return this.expires_in; }
	public String getrefresh_token(){ return this.refresh_token; }
	
	public void setaccess_token(String access_token){ this.access_token = access_token; }
	public void settoken_type(String token_type){ this.token_type = token_type; }
	public void setexpires_in(Date expires_in){ this.expires_in = expires_in; }
	public void setrefresh_token(String refresh_token){ this.refresh_token = refresh_token; }
	
	//Default Constructor
	public Token(){
		
	}
	
	
	public Token(String envName) throws Exception{
		JSONObject tokenObject = getTokenInfo(envName);
		
		if (tokenObject.length() == 0){
			tokenObject = createTokenInfo(envName);
		} 
		
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		Date fdate = c.getTime();
		String tokenExpires = tokenObject.get("expires_in").toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
	    Date parsedDate = dateFormat.parse(tokenExpires);
	    
		if (parsedDate.compareTo(fdate) < 0){
			tokenObject = refreshTokenInfo (tokenObject);
		}
		
		setaccess_token(tokenObject.getString("access_token"));
		settoken_type(tokenObject.getString("token_type"));
		String expToken = tokenObject.get("expires_in").toString();
		dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		setexpires_in(dateFormat.parse(expToken));
		setrefresh_token(tokenObject.getString("refresh_token"));
		
	}
	
	
	
	private static JSONObject getTokenInfo(String envName) throws SQLException {
		JSONObject tokenObject = new JSONObject();
		String selectQuery = "select * from token where envName='"+envName+"';";
		JDBCConnection MyDBObjec = new JDBCConnection();
		MyDBObjec.Dataset(connectionURL, driverURL, dbuser, dbpassword);
		ResultSet myData = MyDBObjec.SelectRecords(selectQuery);
		while (myData.next()){
			tokenObject.put("envName", myData.getString("envName"));
			tokenObject.put("access_token", myData.getString("access_token"));
			tokenObject.put("token_type", myData.getString("token_type"));
			tokenObject.put("expires_in", myData.getTimestamp("expires_in"));
			tokenObject.put("refresh_token", myData.getString("refresh_token"));
		}
		return tokenObject;
	}
	
	private static JSONObject refreshTokenInfo (JSONObject tokenObject) throws Exception{
		String envName = tokenObject.getString("envName");
		String refresh_token = tokenObject.getString("refresh_token");
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		String site = envInfo.getString("site");
		String Thrifthost = envInfo.getString("Thrifthost");
		String oathsite = envInfo.getString("oathsite");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String tokenUrl = oathsite + "oauth2/token";
		String scope = "patron";
		String userinfo = "grant_type=refresh_token&scope="+scope+"&refresh_token="+refresh_token;
		String uuid = "1d8cf412-8fca-4b84-bf22-b93737a4bffe";
		String cid = "patron";
		int expTimeInMinutes= 6000;
		
		String iosAppToken = JwtBuilder.generateToken(uuid, cid, expTimeInMinutes);
		String tokenInfo = UtilityTools.postAuthUrl(tokenUrl, iosAppToken, userinfo);
		System.out.println("Token: " + tokenInfo + "\n");
		JSONObject tokenObject2 = new JSONObject(tokenInfo);
		 String access_token = tokenObject2.getString("access_token");
		 String token_type = tokenObject2.getString("token_type");
		 int expires = tokenObject2.getInt("expires_in");
		 refresh_token = tokenObject2.getString("refresh_token");
		 
		 Calendar c = Calendar.getInstance();
		 c.add(Calendar.SECOND, expires);
		 Date expires_in = c.getTime();
		
		Connection con = DriverManager.getConnection(connectionURL, dbuser, dbpassword);
		Statement statement = con.createStatement();
		String insertQuery = "update worklists_token set access_token='" + access_token + "', token_type='" + token_type + "', expires_in='" + expires_in + "', refresh_token='" + refresh_token + "' where envName='" + envName + "'";
		System.out.println(insertQuery + "\n");  
		statement.executeUpdate(insertQuery);
		
		tokenObject = getTokenInfo(envName);
		
		return tokenObject;
	}
	
	private static JSONObject createTokenInfo(String envName) throws Exception{
		
		JSONObject envInfo = UtilityTools.getEnvInfo(envName);
		String site = envInfo.getString("site");
		String Thrifthost = envInfo.getString("Thrifthost");
		String oathsite = envInfo.getString("oathsite");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String scope = "patron";
		String tokenUrl = oathsite + "oauth2/token";
		String userinfo = "grant_type=password&username="+username+"&password="+password+"&site="+libsite+"&scope="+scope;
		String uuid = "1d8cf412-8fca-4b84-bf22-b93737a4bffe";
		String cid = "patron";
		int expTimeInMinutes= 6000;
		
		String iosAppToken = JwtBuilder.generateToken(uuid, cid, expTimeInMinutes);		 
		String tokenInfo = UtilityTools.postAuthUrl(tokenUrl, iosAppToken, userinfo);
		
		 JSONObject tokenObject = new JSONObject(tokenInfo);
		 String access_token = tokenObject.getString("access_token");
		 String token_type = tokenObject.getString("token_type");
		 int expires = tokenObject.getInt("expires_in");
		 String refresh_token = tokenObject.getString("refresh_token");
		 
		 Calendar c = Calendar.getInstance();
		 c.add(Calendar.SECOND, expires);
		 Date expires_in = c.getTime();
		 
		
		 Connection con = DriverManager.getConnection(connectionURL, dbuser, dbpassword);
		 Statement statement = con.createStatement();
		 String insertQuery = "insert into worklists_token (envName, access_token, token_type, expires_in, refresh_token) values ('" +envName + "', '"  + access_token + "', '" + token_type + "', '" + expires_in + "', '" + refresh_token + "');";
		 System.out.println(insertQuery + "\n"); 
		 statement.executeUpdate(insertQuery);
		 
		 tokenObject = getTokenInfo(envName);
		
		return tokenObject;
	}
	

}
