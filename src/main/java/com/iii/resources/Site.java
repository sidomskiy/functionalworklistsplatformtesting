package com.iii.resources;

/**

 * @version $Id: Site.java 245983 2015-04-06 22:43:57Z iii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */
import org.json.JSONObject;


public class Site {
	
	private int id;
	private String siteCode;
	private String siteCategory;
	private String langRegion;
	private String name;
	private String location;
	
	
	public int getId() { return id; }
	public String getSiteCode() { return siteCode; }
	public String getsiteCategory() { return siteCategory; }
	public String getlangRegion() { return langRegion; }
	public String getname() { return name; }
	public String getLocation() { return location; }
	
	
	public void setId(int id) { this.id = id; }
	public void setSiteCode(String siteCode) { this.siteCode = siteCode; }
	public void setsiteCategory(String siteCategory) { this.siteCategory = siteCategory; }
	public void setlangRegion(String langRegion) { this.langRegion = langRegion; }
	public void setName(String name) { this.name = name; }
	public void setLocation(String location) { this.location = location; }
	
	
	public Site(){
		
	}
	
	public Site(JSONObject childSiteObject){
		
		setId(childSiteObject.getInt("id"));
		setSiteCode(childSiteObject.getString("siteCode"));
		setsiteCategory(childSiteObject.optString("siteCategory"));
		setlangRegion(childSiteObject.optString("langRegion"));
		setName(childSiteObject.getString("name"));
		setLocation(childSiteObject.optString("location"));
		
		
	}

}
