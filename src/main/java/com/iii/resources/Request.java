package com.iii.resources;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONObject;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.iii.common.JwtBuilder;
import com.iii.common.UtilityTools;

public class Request {
	static String addItemEntryWorklist = null;
	public Request() throws Exception {
		/*
		String site = "https://qa-api.iii.com/worklists/v3/";
		String Thrifthost = "qa-sierra-app04.iii.com";
		String oathsite = "https://qa-api.iii.com/auth/v1/";
		String username = "autotest";
		String password = "test";
		String site_id = "qapl4";
		String id = "6802145";
		String[] barcodeNums = {"12932620", "12932661", "12932679", "20066643", "21904008", "12933032", "12933107", "12933131", "14011720", "15234198", "17738493", "12933164", "19650100", "18947556", "12933271", "12933347", "12933362", "12933412", "12933420", "12933438", "12933479"};
		String newItemWorklist = site + "itemworklists/";
		String itemWorklistName = "AutomationTestWorklist "+ getDateTime();
		JSONObject newItmWkl = new JSONObject();
		newItmWkl.put("userId", id);
		newItmWkl.put("name", itemWorklistName);
		 
		String post_json = newItmWkl.toString();
		JSONObject NewItmWkl =  streamJObjectPost(newItemWorklist, post_json, site_id);
		
		int newId = NewItmWkl.getInt("id");
		String itemWorklistId = Integer.toString(newId);
		
		for (int i=0; i< barcodeNums.length; i++){
			// Creating a addItemWorklist json
				JSONObject obj=new JSONObject();
			// add barcode	
				obj.put("barcode", barcodeNums[i]);
			// Creating json string
				String json_string = obj.toString();
			// adding worklist id to url
				   String myAddItemToWorklist = newItemWorklist + itemWorklistId + "/items";
			// Adding an item-entry to a worklist
				String addItemEntryWorklist = streamUrlPost(myAddItemToWorklist, json_string, site_id);
				
		

		}
		*/
		System.out.println("Starting new process");
    }
	
	public void doWork() {
		System.out.println("Starting doWork process");
	}
	
	public static String getDateTime() {
		String dateFormatNow = "HHmmssSSS";
		String dateTimeStr = null;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormatNow);
		dateTimeStr = sdf.format(cal.getTime());
		return dateTimeStr;
	}
	
	public static JSONObject streamJObjectPost (String urlString, String json_string, String testSite) throws Exception {
		
		StringRequestEntity requestEntity = new StringRequestEntity(json_string, "application/json", "UTF-8");
		HttpClient client = new HttpClient();
		PostMethod postApiResp = new PostMethod(urlString);
		postApiResp.addRequestHeader("Authorization", ("Bearer " + getAuthenticationToken(testSite)));
		postApiResp.setRequestEntity(requestEntity);
		try {
			int returnCode = client.executeMethod(postApiResp);
			//Assert.assertEquals(201, returnCode);
		} catch (Exception e) {
			
			System.err.print("Error to post: " + e.toString() +"\n");

		}
		
		//System.out.println("Return code: " + returnCode + "\n"); 
		BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		JSONObject jsonRObject = new JSONObject(inputStr);
		
		return jsonRObject;
	}
	
	public static String streamUrlPost (String urlString, String json_string, String testSite) throws Exception {
		String inputStr = null;
		try {
			StringRequestEntity requestEntity = new StringRequestEntity(json_string, "application/json", "UTF-8");
			HttpClient client = new HttpClient();
			PostMethod postApiResp = new PostMethod(urlString);
			postApiResp.addRequestHeader("Authorization", ("Bearer " + getAuthenticationToken(testSite)));
			postApiResp.setRequestEntity(requestEntity);
			int returnCode = client.executeMethod(postApiResp);
			if (returnCode == 200){
				BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
				inputStr=readAll(br);
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
				inputStr=returnCode + " | "+ readAll(br);
			}
		} catch (Exception e) {
			System.out.println("error from Utility tool: " + e.toString() + "\n");
			e.printStackTrace();
			inputStr = e.toString();

		}
		return inputStr;
	}
	
	public static String readAll(Reader br) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = br.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
	
	public static boolean writeStrToFile(String str, String file) {
		BufferedWriter out;
		try {
			out = new BufferedWriter(new FileWriter(file, true));
			out.write(str);
			out.close();
			return true;
		} catch (IOException e) {
			System.err.println("exception returned: " + e);
			return false;
		}
	}
	
public static String getAuthenticationToken(String testSite) throws Exception{
		
		String platformInfo = "environments.xml";
		String site = "";
		String username = "";
		String password ="";
		String libsite = "";
		String token ="";
		try {
			 File file = new File(platformInfo);
			 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			 DocumentBuilder db = dbf.newDocumentBuilder();
			 Document document = db.parse(file);
			 document.getDocumentElement().normalize();
			 NodeList node = document.getElementsByTagName(testSite);
			 
			 Node nNode = node.item(0);
			 Element eElement = (Element) nNode;
			 
			 
			 site = getTagValue("oathsite", eElement);
			 username = getTagValue("username", eElement);
			 password = getTagValue("password", eElement);
			 libsite = getTagValue("libsite", eElement);
			 
		} catch (Exception e) {
			System.err.println("unable to open env info: " + e.toString());

		}
	
		String scope = "worklist";
		String tokenUrl = site + "oauth2/token";
		String userinfo = "grant_type=password&username="+username+"&password="+password+"&site="+libsite+"&scope="+scope;
		String uuid = "1d8cf412-8fca-4b84-bf22-b93737a4bffe";
		String cid = "worklists";
		int expTimeInMinutes= 36000;
		

		String iosAppToken = JwtBuilder.generateToken(uuid, cid, expTimeInMinutes);
		try {
		String staffTokenInfo = UtilityTools.postAuthUrl(tokenUrl, iosAppToken, userinfo);
		//System.out.println("Staff Token Info: "+ staffTokenInfo + "\n");
		JSONObject tokenJobject = new JSONObject(staffTokenInfo);
		token = tokenJobject.getString("access_token");
		}catch (Exception e) {
			System.err.println("unable to create token: " + e.toString());

		}
		
		return token;
	}

	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
	 
	        Node nValue = (Node) nlList.item(0);
	 
		return nValue.getNodeValue();
	}

}
