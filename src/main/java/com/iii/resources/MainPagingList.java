package com.iii.resources;
/**

 * @version $Id: MainPagingList.java 245983 2015-04-06 22:43:57Z iii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */

import java.util.Map;
import com.iii.common.*;

public class MainPagingList {
	private String pagingEntriesClass;
	private Number id;
	private String author;
	private String callNumber;
	private String imageUrl;
	private String location;
	private Map <String, String> masterPagingList;
	private String publicationDate;
	private String publicationInfo;
	private String recordNumber;
	private Map <String, String> state;
	private String title;
	
	public String getpagingEntriesClass(){
		return this.pagingEntriesClass;
	}
	public void setpagingEntriesClass(String pagingEntriesClass){
		this.pagingEntriesClass = pagingEntriesClass;
	}
	
	public Number getid(){
		return this.id;
	}
	public void setid(Number id){
		this.id = id;
	}
	
	public String getauthor(){
		return this.author;
	}
	public void setauthor(String author){
		this.author = author;
	}
	
	public String getcallNumber(){
		return this.callNumber;
	}
	public void setcallNumber(String callNumber){
		this.callNumber = callNumber;
	}
	
	public String getimageUrl(){
		return this.imageUrl;
	}
	public void setimageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}
	
	public String getlocation(){
		return this.location;
	}
	public void setlocation(String location){
		this.location = location;
	}
	
	public Map <String, String> getmasterPagingList(){
		return this.masterPagingList;
	}
	public void setmasterPagingList(Map <String, String> masterPagingList){
		this.masterPagingList = masterPagingList;
	}
	
	public String getpublicationDate(){
		return this.publicationDate;
	}
	public void setpublicationDate(String publicationDate){
		this.publicationDate = publicationDate;
	}
	
	public String getpublicationInfo(){
		return this.publicationInfo;
	}
	public void setpublicationInfo(String publicationInfo){
		this.publicationInfo = publicationInfo;
	}
	
	public String getrecordNumber(){
		return this.recordNumber;
	}
	public void setrecordNumber(String recordNumber){
		this.recordNumber = recordNumber;
	}
	
	public Map <String, String> getstate(){
		return this.state;
	}
	public void setstate(Map <String, String> state){
		this.state = state;
	}
	
	public String gettitle(){
		return this.title;
	}
	public void settitle(String title){
		this.title = title;
	}
	
	
	//Default Constructor
	public MainPagingList(){
				
	}
		
	public MainPagingList(String sitecode){
			
		
			
	}
	

}
