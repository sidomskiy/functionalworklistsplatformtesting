package com.iii.resources;
/**

 * @version $Id: MyRunnable.java 245983 2015-04-06 22:43:57Z iii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONObject;
import com.iii.common.UtilityTools;


public class MyRunnable implements Runnable {
	private final long countUntil;
	static String siteURL= "";
	static String iosAppToken = "IIIJWT ewogICJ0eXAiIDogIkpXVCIsCiAgImFsZyIgOiAiSFM1MTIiCn0=.ewogICJleHAiIDogMTQ1MzkzNjg2MywKICAiaWlpIiA6ICJlNmNHSmNFcHhqdk9QOElIMmppUkRiQzBSalM3K3o2NGMrTUJNMTZHcVBDXC85OWVmYTczb2ZwN2pVYnFUd3dkWUU4cTBoZWc3WldFZ0kxakUzc0M1ZVAwKzZ0ZllOb0lDcENnZUc0dVFYRTB4d211OXdqK25UZjhCRjFtcm9GbmJHcmJ3QWJpRTBVYTBYUEdZa2tqWjB6WjgwYW9yaVo5YUR5QXM5bGNBSnFiY25GK08wbDByYkE3akdFR0U3YitMcmlTZ2xBYnVtcExieURScmZabXg5Zkhob3Y0SGRcL2VEUVwvTDJWdUlBbkZ5ZmJRYWVVK0Z3SGd5TTB3SE8yc0lOSHQydmNmekU5R0dRSURSWjBFVVVhVVUwd1VsSXh1Yk9DZFAyVnpBeVBZMkdVeDN0dkhUYW91cFBhYmtoM3lnYVFpWDBEZHVYYkFHN0t5Q1hXeEdQYXc9PSIsCiAgImlzcyIgOiAiMWJiMGM5MWItZmJkMS00Y2IzLWE2Y2MtZDE1ZTRlYTBkNzk0IiwKICAiaWF0IiA6IDE0MjI0MDA4NjMKfQ==.ACTMWrsY+zj2YXfsUFXEF9jVG9ryrGJ9FfrmZf0si188B0h4Q2mUbEOAxEFWA/MkbzdkzzrubmE2n7ygYF4GBQ==";
	static String infoMy = "grant_type=client_credentials&scope=worklist";
	static String autUrl = "";
	static String iosVersion = "1";
	static String site = "http://iii-staging-elb-473342976.us-west-1.elb.amazonaws.com/worklists/v1/";
	static String username = "Serge";
	static String password = "serge";
	static String libsite = "iiipl";
	static String baseUrl = "http://iii-staging-elb-473342976.us-west-1.elb.amazonaws.com/";
	static String myBearerToken = "";
	static String itemWorklistId = "";
	static String testSite = "staging";
	static String newItemWorklist ="http://iii-staging-elb-473342976.us-west-1.elb.amazonaws.com/worklists/v1/itemworklists";
	
	// staging: "http://iii-staging-elb-473342976.us-west-1.elb.amazonaws.com/"  
	// qa: "http://ec2-54-67-114-220.us-west-1.compute.amazonaws.com/"

  public MyRunnable(long countUntil) throws Exception {

	    siteURL = baseUrl + "worklists/v1/versions";
	    autUrl = baseUrl + "auth/v1/oauth2/token";
	    site = baseUrl + "worklists/v1/";
	    String urlString = baseUrl + "itemworklists/";
	    String scope = "worklist";
	    String[] myBarcodeNum = {"12932620", "12932661", "12932679", "20066643", "21904008", "12933032", "12933107", "12933131", "14011720", "15234198", "17738493", "12933164", "19650100", "18947556", "12933271", "12933347", "12933362", "12933412", "12933420", "12933438", "12933479", "14970586", "12933537", "14011753", "16503369", "12933651", "12933792", "14011761", "12933834", "12933859", "12933867", "14011787", "12933933", "12934139", "12934154", "14970669", "12934170", "18811919", "18623629", "75394593"};
		
	  //create new item worklist
		 JSONObject newItmWkl = new JSONObject();
		 String itemWorklistName = "AutoTest "+ getDateTime();
		 String userId = getUserId();
		 newItmWkl.put("userId", userId);
		 newItmWkl.put("name", itemWorklistName);
		 
		 String post_json = newItmWkl.toString();
		 JSONObject NewItmWkl =  UtilityTools.streamJObjectPost(newItemWorklist, post_json, testSite);
		 
		 int newId = NewItmWkl.getInt("id");
		 itemWorklistId = Integer.toString(newId);
	 // add items to worklist
		 
		 for (int i=0; i< myBarcodeNum.length; i++){
				// Creating a addItemWorklist json
					
					JSONObject obj=new JSONObject();
					obj.put("barcode", myBarcodeNum[i]);
					String json_string = obj.toString();
				    String myAddItemToWorklist = newItemWorklist + "/" + itemWorklistId + "/items";
				    System.out.print(myAddItemToWorklist + "\n");
					String addItemEntryWorklist = UtilityTools.streamUrlPost(myAddItemToWorklist, json_string, testSite);
					System.out.print(addItemEntryWorklist + "\n");	
			 }
		
		
	  this.countUntil = countUntil;
  }
  
  public static String readAll(Reader br) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = br.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	}
  public static String getDateTime() {
		String dateFormatNow = "HHmmssSSS";
		String dateTimeStr = null;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormatNow);
		dateTimeStr = sdf.format(cal.getTime());
		return dateTimeStr;
	}
  public static String getUserId() throws Exception{
	  
	  String myUserInfoUrl = site + "users?sitecode=" + libsite + "&username=" + username;
	  String myStringAllUsersJson = UtilityTools.streamStringUrl(myUserInfoUrl, testSite);
	  JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
	  //System.out.print(myUserInfoObject.toString() + "\n");
	  String id = myUserInfoObject.optString("id");
	  return id;
  }

  //@Override
  public void run() {
    long sum = 0;
    for (long i = 1; i < countUntil; i++) {
      sum += i;
    }
    System.out.println(sum);
  }
} 
