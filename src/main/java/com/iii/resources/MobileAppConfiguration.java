package com.iii.resources;
import org.json.JSONObject;
public class MobileAppConfiguration {
	
	private String iosPatronVersion;
	private String iosWorklistsVersion;
	
	public String getiosPatronVersion() { return iosPatronVersion; }
	public String getiosWorklistsVersiony() { return iosWorklistsVersion; }
	
	public void setiosPatronVersion(String iosPatronVersion) { this.iosPatronVersion = iosPatronVersion; }
	public void setiosWorklistsVersion(String iosWorklistsVersion) { this.iosWorklistsVersion = iosWorklistsVersion; }
	
	public MobileAppConfiguration(){
		
	}

	public MobileAppConfiguration(JSONObject mobAppCon){
		setiosPatronVersion(mobAppCon.getString("iosPatronVersion"));
		setiosWorklistsVersion(mobAppCon.getString("iosWorklistsVersion"));
		
	}

}
