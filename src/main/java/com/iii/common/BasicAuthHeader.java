package com.iii.common;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

public class BasicAuthHeader {
	
	private BasicAuthHeader(){
		
		
	}
	
	public static String generatebasicheader (String key, String secret ){
		String headerString="";
		String keySecret = key+":"+ secret;
		byte[] bkey = null;
		byte[] bsecret = null;
		try {
			bkey = (keySecret.toString()).getBytes("UTF-8");
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String b64Key = Base64.encodeBase64String(bkey);
		
		headerString = b64Key;
		return headerString;
	}
	

}
