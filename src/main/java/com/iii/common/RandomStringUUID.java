package com.iii.common;
/**

 * @version $Id: RandomStringUUID.java 245352 2015-03-18 16:49:56Z iii $ $Rev: 244203 $ $Date: 2015-02-04 15:18:26 -0800 (Wed, 04 Feb 2015) $

 */
import java.util.UUID;

public class RandomStringUUID {
	
	public static String createClientUuid(){
		
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
		
		return randomUUIDString;
	}

}
