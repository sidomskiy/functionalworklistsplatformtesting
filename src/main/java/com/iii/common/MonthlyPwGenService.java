package com.iii.common;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MonthlyPwGenService {
	static int[] fnVec = new int[] {1,4,7,3,9,2,5,4,6,8};
	
	public static String getMonthlyPw() {
		Calendar cal = getCalendarSeed();
		
		cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        
        
		int nTime = ((int)(cal.getTimeInMillis() / 1000)) % 2027;
		int nNum = nTime + (10000 * fnVec[nTime  % 10]);
		
		nNum += 999;
		nNum = (Math.abs(nNum * nNum) % 90000) + 10000;
		
		return String.valueOf(nNum);
	}
	
	public static Calendar getCalendarSeed() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        
        return cal;
    }

}
