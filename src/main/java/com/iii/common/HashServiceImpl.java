package com.iii.common;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.apache.commons.codec.binary.Base64;

public class HashServiceImpl {
	
	public static String main(int cid, String key, String secret) throws NoSuchAlgorithmException, InvalidKeySpecException {
		
			
		SecureRandom random = new SecureRandom();
	    byte salt[] = new byte[16];
	    random.nextBytes(salt);
	    System.out.print(hash(secret, salt));
	    System.out.print(hash(key, salt));
	    return "" + cid + "." + hash(key, salt) + "." + hash(secret, salt);
	}

	private static String hash(String secret, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
		PBEKeySpec spec = new PBEKeySpec(secret.toCharArray(), salt, 65536, 128);
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] hash = f.generateSecret(spec).getEncoded();
		return HashedSecret(1, salt, hash);
	}

	private static String HashedSecret(int i, byte[] salt, byte[] hash) {
		int version = i;
		salt = Base64.decodeBase64(salt.toString());
		hash = Base64.decodeBase64(hash.toString());
		return HashedSecret(version, salt, hash);
	}
	
	
	

}
