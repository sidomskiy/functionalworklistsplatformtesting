package com.iii.common;


/**

 * @version $Id: UtilityTools.java 247939 2015-06-01 17:30:14Z sergeii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import com.iii.common.JwtBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import junit.framework.Assert;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
@SuppressWarnings("all")

public class UtilityTools {
	static String envInformation = "environments.xml";
	//static String iosAppToken = "IIIJWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiIxZDhjZjQxMi04ZmNhLTRiODQtYmYyMi1iOTM3MzdhNGJmZmUiLCJpaWkiOiJHNzU4RHNFN3Flbi9jTzFadGwwdXFtZVh2UDVsMUI4WlN6aEVwdmZWWnJaKzJUQWVsMC82YTRIWDJhNS9LM2h4V1MxK3dobEEwQjhZM24raEoydXlKTER6VzdXS1FOcHNqdFlVM1YvdTV0bHV0Q3lkOFFrNGVrbEJCUGZKMVpGaDgyNnA2QkVoeWJLSG9ReW5LeVNxUHk1Z1RiYWdqa2xMU0M1T1g5RmxnZ0FCWWJwRUJQRlhaaXc5elp5MXV5eWFpWE9VcmtLUllmQ2tYazdzMDlVRDBnK08yN1V3a1o0dk9Zd0tWZEdmdWJ4OHZEODltT081dWF5WWpjNlZObGVUM0pSZk5VQVZxNjR0WG5tbkRxejdhK1N3UHpIc1Y3clR3MzhQdER2ZjFHYVhwanVQclpKNE1JZytVdWp4SS9HUEFaUDdEQ3R2SGtCMTdHNy9XaVY3UkE9PSIsImV4cCI6MTQ2ODA4MzcwNywiaWF0IjoxNDY1OTIzNzA3LCJjaWQiOjF9.Qkezq/wjo2ukCSecsPpjorn9SWT9sbq17wr3mfNxMH8NFRfxmbEizhJE2NKscWpBgfnMAhDwIggjAppAqR9oCw==";
	
	public static int getItemWorklistId (String site, String userId, String testSite) throws Exception {
		String myItemsUrl = site + "itemworklists?user=" + userId;
		int itemWorklistId = 0;
		
		String itemWorklistItems = streamUrl(myItemsUrl, testSite);
		JSONArray myWorklistItems = new JSONArray(itemWorklistItems);
		int numOfItems = myWorklistItems.length();
		JSONObject WorklistItem = myWorklistItems.getJSONObject(numOfItems -1);
		itemWorklistId = WorklistItem.getInt("id");

		return itemWorklistId;
	}
	
	public static String streamUrl (String urlString, String testSite) throws Exception {
		String inputStr ="";
		HttpClient client = new HttpClient();
		GetMethod getApiResp = new GetMethod(urlString);
		getApiResp.addRequestHeader("Authorization", ("Bearer " + getAuthenticationToken(testSite)));
		int returnCode = client.executeMethod(getApiResp);
		
		if (returnCode == 200){
			BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
			inputStr=readAll(br);
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
			inputStr=returnCode + " : " + readAll(br);
		}
		
		return inputStr;
	}
	
	public static String streamAuthUrl (String urlString, String token) throws Exception {
		
		HttpClient client = new HttpClient();
		GetMethod getApiResp = new GetMethod(urlString);
		getApiResp.addRequestHeader("IIIJWT", token);
		int returnCode = client.executeMethod(getApiResp);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		return inputStr;
	}
	
	public static String streamBearerToken (String urlString, String token) throws Exception {
		
		HttpClient client = new HttpClient();
		GetMethod getApiResp = new GetMethod(urlString);
		getApiResp.addRequestHeader("Authorization", "Bearer "+token);
		//getApiResp.addRequestHeader("Bearer", token);
		int returnCode = client.executeMethod(getApiResp);
		System.out.print(returnCode + "\n");
		BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		return inputStr;
	}
	public static String getAuthenticationToken(String envName) throws Exception{
		
		String token = "";

			JSONObject envInfo = getEnvInfo(envName);
			 
			String site = envInfo.getString("site");
			String oathsite = envInfo.getString("oathsite");
			String Thrifthost= envInfo.getString("Thrifthost");
			String username = envInfo.getString("username");
			String password = envInfo.getString("password");
			String libsite = envInfo.getString("libsite");
			 
		
	
		String scope = "worklist";
		String tokenUrl = oathsite + "oauth2/token";
		String userinfo = "grant_type=password&username="+username+"&password="+password+"&site="+libsite+"&scope="+scope;
		String uuid = "1d8cf412-8fca-4b84-bf22-b93737a4bffe";
		String cid = "worklists";
		int expTimeInMinutes= 36000;
		

		//String iosAppToken = JwtBuilder.generateToken(uuid, cid, expTimeInMinutes);
		String iosAppToken = JwtBuilder.generateToken(uuid, cid, expTimeInMinutes);
		
		try {
		String staffTokenInfo = UtilityTools.postAuthUrl(tokenUrl, iosAppToken, userinfo);
		//System.out.println("Staff Token Info: "+ staffTokenInfo + "\n");
		JSONObject tokenJobject = new JSONObject(staffTokenInfo);
		token = tokenJobject.getString("access_token");
		}catch (Exception e) {
			System.err.println("unable to create token: " + e.toString());

		}
		
		return token;
	}
	
	public static String getAppAuthToken(String testSite) throws Exception{
		
		String token ="";
		JSONObject envInfo = UtilityTools.getEnvInfo(testSite);
		 
		String site = envInfo.getString("site");
		String oathsite = envInfo.getString("oathsite");
		String Thrifthost= envInfo.getString("Thrifthost");
		String username = envInfo.getString("username");
		String password = envInfo.getString("password");
		String libsite = envInfo.getString("libsite");
		
		String scope = "worklist";
		String tokenUrl = oathsite + "oauth2/token";
		String userinfo = "grant_type=client_credentials&scope="+scope;
		
		String uuid = "1d8cf412-8fca-4b84-bf22-b93737a4bffe";
		String cid = "worklists";
		int expTimeInMinutes= 36000;
		
		String iosAppToken = JwtBuilder.generateToken(uuid, cid, expTimeInMinutes);
		//System.out.println("app Token: "+ iosAppToken + "\n");
		try {
			String staffTokenInfo = UtilityTools.postAuthUrl(tokenUrl, iosAppToken, userinfo);
			//System.out.println("app Token Info: "+ staffTokenInfo + "\n");
			JSONObject tokenJobject = new JSONObject(staffTokenInfo);
			token = tokenJobject.getString("access_token");
			}catch (Exception e) {
				System.err.println("unable to create token: " + e.toString());

			}
		
		return token;
	}
	public static String getAuthUrl (String urlString, String token) throws Exception {
		String respStr = "";
		HttpClient client = new HttpClient();
		GetMethod getApiResp = new GetMethod(urlString);
		getApiResp.addRequestHeader("Authorization", token);
		try {
			int returnCode = client.executeMethod(getApiResp);
			BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
			respStr=readAll(br);
		} catch (Exception e) {
			System.out.println("Unable to get authorize endpoint: " + e.toString() + "\n");
			respStr = "Unable to get authorize endpoint: " + e.toString();

		}
		return respStr;
	}
	public static String postAuthUrl (String urlString, String token, String userinfo) throws Exception {
		StringRequestEntity requestEntity = new StringRequestEntity(userinfo, "application/x-www-form-urlencoded", "UTF-8");
		HttpClient client = new HttpClient();
		PostMethod getApiResp = new PostMethod(urlString);
		getApiResp.addRequestHeader("Authorization", token);
		getApiResp.setRequestEntity(requestEntity);
		int returnCode = client.executeMethod(getApiResp);
		//System.out.print("return code: " + returnCode + "\n");
		BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		return inputStr;
	}
	
	public static String postBearerToken (String urlString, String token, String userinfo) throws Exception {
		StringRequestEntity requestEntity = new StringRequestEntity(userinfo, "application/x-www-form-urlencoded", "UTF-8");
		HttpClient client = new HttpClient();
		PostMethod getApiResp = new PostMethod(urlString);
		getApiResp.addRequestHeader("Bearer", token);
		getApiResp.setRequestEntity(requestEntity);
		int returnCode = client.executeMethod(getApiResp);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		return inputStr;
	}
	public static String streamPost (String urlString, String testSite) throws Exception {
		
		String inputStr = null;
		
		try {
			HttpClient client = new HttpClient();
			PostMethod postApiResp = new PostMethod(urlString);
			postApiResp.addRequestHeader("Authorization", ("Bearer " + getAuthenticationToken(testSite)));
			int returnCode = client.executeMethod(postApiResp);
			if (returnCode == 200){
				BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
				inputStr=readAll(br);
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
				inputStr=returnCode + " | "+ readAll(br);
			}
			
		} catch (Exception e) {
			System.out.println("\tunable to sending a list of items to ILS: " + e + "\n");
			e.printStackTrace();
			inputStr = e.toString();

		}
		
		
		return inputStr;
	}
	public static String streamUrlPost (String urlString, String json_string, String testSite) throws Exception {
		String inputStr = null;
		try {
			StringRequestEntity requestEntity = new StringRequestEntity(json_string, "application/json", "UTF-8");
			HttpClient client = new HttpClient();
			PostMethod postApiResp = new PostMethod(urlString);
			
			postApiResp.addRequestHeader("Authorization", ("Bearer " + getAuthenticationToken(testSite)));
			postApiResp.setRequestEntity(requestEntity);

			int returnCode = client.executeMethod(postApiResp);
			if ((returnCode == 200)||(returnCode == 202)){
				BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
				inputStr=readAll(br);
			} else if (returnCode == 204) {
				inputStr=Integer.toString(returnCode);
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
				inputStr=returnCode + " | "+ readAll(br);
			}
		} catch (Exception e) {
			System.out.println("error from Utility tool: " + e.toString() + "\n");
			e.printStackTrace();
			inputStr = e.toString();

		}
		return inputStr;
	}
	
	public static String streamUrlPut (String urlString, String json_string, String testSite) throws Exception {
		String responseBody = null;
		int responseCode = -1;
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPut httpPut = new HttpPut(urlString);
            httpPut.setEntity(new StringEntity(json_string));
            httpPut.setHeader("Authorization", ("Bearer " + UtilityTools.getAuthenticationToken(testSite)));

           // System.out.println("Token: " + "Bearer " + UtilityTools.getAuthenticationToken(testSite));

            // Create a custom response handler
            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            };
            responseBody = httpclient.execute(httpPut, responseHandler);
            
        }catch (Exception e) {
			
			e.printStackTrace();

		}
		return responseBody;
	}
	
	public static int streamUrlDelete (String urlString, String testSite) throws Exception {
		
		HttpClient client = new HttpClient();
		DeleteMethod deleteApiResp = new DeleteMethod(urlString);
		deleteApiResp.addRequestHeader("Authorization", ("Bearer " + getAuthenticationToken(testSite)));
		int returnCode = client.executeMethod(deleteApiResp);
		if(returnCode == 403){
			BufferedReader br = new BufferedReader(new InputStreamReader(deleteApiResp.getResponseBodyAsStream()));
			String inputStr=readAll(br);
			System.err.print(inputStr + "\n");
		}
		return returnCode;
	}
	
	

	public static JSONObject streamJObjectPost (String urlString, String json_string, String testSite) throws Exception {
		
		StringRequestEntity requestEntity = new StringRequestEntity(json_string, "application/json", "UTF-8");
		HttpClient client = new HttpClient();
		PostMethod postApiResp = new PostMethod(urlString);
		postApiResp.addRequestHeader("Authorization", ("Bearer " + getAuthenticationToken(testSite)));
		postApiResp.setRequestEntity(requestEntity);
		try {
			int returnCode = client.executeMethod(postApiResp);
			//Assert.assertEquals(201, returnCode);
		} catch (Exception e) {
			
			System.err.print("Error to post: " + e.toString() +"\n");

		}
		
		//System.out.println("Return code: " + returnCode + "\n"); 
		BufferedReader br = new BufferedReader(new InputStreamReader(postApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		JSONObject jsonRObject = new JSONObject(inputStr);
		
		return jsonRObject;
	}

	public static JSONObject requestingToken(String authLogin, String tokenUrl, String host) throws HttpException, IOException, JSONException{
		
		String credentials =  new String(Base64.encodeBase64(authLogin.toString().getBytes()));
		
		
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(tokenUrl);
		
		post.setRequestHeader("Host", host);
		post.setRequestHeader("Authorization", "Basic " + credentials);
		post.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		System.out.println("Header: "+ tokenUrl + " | " + host + " | Basic " + credentials + "\n");
			
		int returnCode = client.executeMethod(post);
		
		
		JSONObject jsonRObject = new JSONObject(post.getResponseBodyAsString());
		
		
		
		return jsonRObject;
		
	}
	

	public static JSONObject readUrl(String urlString) throws Exception {
		
				
		HttpClient client = new HttpClient();
		CookieManager cm = new CookieManager();
	    cm.setCookiePolicy(CookiePolicy.ACCEPT_NONE);
	    CookieHandler.setDefault(cm);
				
		GetMethod getApiResp = new GetMethod(urlString);
		int returnCode = client.executeMethod(getApiResp);
		JSONObject jsonRObject = new JSONObject(getApiResp.getResponseBodyAsString());
		
		
		return jsonRObject;
	}
	public static JSONObject streamJobjectUrl (String urlString) throws Exception {
		
		HttpClient client = new HttpClient();
		GetMethod getApiResp = new GetMethod(urlString);
		int returnCode = client.executeMethod(getApiResp);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		int startPoint = inputStr.indexOf("[");
		int endPoint = inputStr.lastIndexOf("]");
		//System.out.println(startPoint +" | "+ endPoint + "\n");
		//System.out.println("json string: "+ inputStr + "\n");
		if (startPoint > 0){
			
			inputStr = inputStr.substring((startPoint+1), (endPoint-1));
			
		}
		//System.out.println(inputStr + "\n");
        JSONObject jsonRObject = new JSONObject(inputStr);
		return jsonRObject;
	}
	public static String streamStringUrl (String urlString, String testSite) throws Exception {
		String inputStr= "";
		HttpClient client = new HttpClient();
		GetMethod getApiResp = new GetMethod(urlString);
		String token = "Bearer " + getAuthenticationToken(testSite);
		getApiResp.addRequestHeader("Authorization", (token));
		int returnCode = client.executeMethod(getApiResp);
		if ((returnCode == 200) ||(returnCode == 207)){
			BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
			inputStr=readAll(br);
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
			inputStr=returnCode + " : " + token + " | "+ readAll(br);
		}
		return inputStr;
	}
	public static JSONObject streamReadUrl(String urlString, String apiToken) throws Exception {
		
		String headerValue = "Bearer " + apiToken;
		HttpClient client = new HttpClient();
		GetMethod getApiResp = new GetMethod(urlString);
		getApiResp.addRequestHeader("Authorization", headerValue);
		int returnCode = client.executeMethod(getApiResp);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		
        JSONObject jsonRObject = new JSONObject(inputStr);
		return jsonRObject;
	}
	
	public static String streamUrlAsString (String urlString, String apiToken) throws Exception {
		
		String headerValue = "Bearer " + apiToken;
		HttpClient client = new HttpClient();
		GetMethod getApiResp = new GetMethod(urlString);
		getApiResp.addRequestHeader("Authorization", headerValue);
		int returnCode = client.executeMethod(getApiResp);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
		String inputStr=readAll(br);
		return inputStr;
	}
	
	public static String getSierraApiToken(String envName) throws ParserConfigurationException, SAXException, IOException{
		JSONObject getEnvInfo = getEnvInfo(envName);
		String site = getEnvInfo.getString("site");
		String Thrifthost = getEnvInfo.getString("Thrifthost");
		String oathsite = getEnvInfo.getString("oathsite");
		String username = getEnvInfo.getString("username");
		String password = getEnvInfo.getString("password");
		String libsite = getEnvInfo.getString("libsite");
		String threeLetters ="";
		if (libsite.equals("qapl") || libsite.equals("qapl3") || libsite.equals("qapl4")){
			
			threeLetters = "qa-";
		}else{
			threeLetters = libsite.substring(0, 3);
		}
		String tokenUrl = "https://" + Thrifthost + "/iii/sierra-api/v3/token";
	    String authLogin = "iii:" + MonthlyPwGenService.getMonthlyPw() + threeLetters;
	    
	    String credentials =  new String(Base64.encodeBase64(authLogin.toString().getBytes()));
		
		
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(tokenUrl);
		
		post.setRequestHeader("Host", Thrifthost);
		post.setRequestHeader("Authorization", "Basic " + credentials);
		post.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			
		int returnCode = client.executeMethod(post);
					
		JSONObject jsonRObject = new JSONObject(post.getResponseBodyAsString());
		
		String token = jsonRObject.getString("access_token");
		
		return token;
	}
	
	public static String streamStringAPIUrl (String urlString, String testSite) throws Exception {
		String inputStr= "";
		HttpClient client = new HttpClient();
		GetMethod getApiResp = new GetMethod(urlString);
		String token = "Bearer " + getSierraApiToken(testSite);
		getApiResp.addRequestHeader("Authorization", (token));
		int returnCode = client.executeMethod(getApiResp);
		if ((returnCode == 200) ||(returnCode == 207)){
			BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
			inputStr=UtilityTools.readAll(br);
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(getApiResp.getResponseBodyAsStream()));
			inputStr=returnCode + " : " + token + " | "+ UtilityTools.readAll(br);
		}
		return inputStr;
	}
	
	public static String getUserId (String site, String envName, String site_id, String username) throws Exception {
		String myUserId = "";
		String myUserInfoUrl = site + "users?sitecode="+site_id+"&username="+username;
		String myStringAllUsersJson = streamStringUrl(myUserInfoUrl, envName);
		//System.out.print("User info" + myStringAllUsersJson + "\n");
		JSONObject myUserInfoObject =  new JSONObject(myStringAllUsersJson);
		
		myUserId = myUserInfoObject.optString("id");
		
		return myUserId;
	}
	
	public static String readAll(Reader br) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = br.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
	
	public static boolean writeStrToFile(String str, String file) {
		BufferedWriter out;
		try {
			out = new BufferedWriter(new FileWriter(file, true));
			out.write(str);
			out.close();
			return true;
		} catch (IOException e) {
			System.err.println("exception returned: " + e);
			return false;
		}
	}
	
	public static boolean overwritText(String str, String file) {
		
		try {
			FileWriter out = new FileWriter(file, false);
			out.write(str);
			out.close();
			return true;
		} catch (IOException e) {
			System.err.println("exception returned: " + e);
			return false;
		}
		
	}
	public static void MySleep(long sleepTime) {
       
        // pause for a while
        Thread thisThread = Thread.currentThread();
        try
            {
            Thread.sleep(sleepTime);
        }
        catch (Throwable t)
            {
            throw new OutOfMemoryError("An Error has occured");
        }
       
       
    }
	
	public static String getDateTime() {
		String dateFormatNow = "HHmmssSSS";
		String dateTimeStr = null;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormatNow);
		dateTimeStr = sdf.format(cal.getTime());
		return dateTimeStr;
	}
	
	public static String getTimeStamp (){
		String timestamp = null;
		String dateFormatNow = "YYYYMMDDHHMMSS";
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormatNow);
		timestamp = sdf.format(cal.getTime());
		return timestamp;
	}
	public static JSONObject getEnvInfo(String envName) throws ParserConfigurationException, SAXException, IOException{
		File file = new File(envInformation);
		 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		 DocumentBuilder db = dbf.newDocumentBuilder();
		 Document document = db.parse(file);
		 document.getDocumentElement().normalize();
		 NodeList node = document.getElementsByTagName(envName);
		 
		 Node nNode = node.item(0);
		 Element eElement = (Element) nNode;
		 
		 
		 String site = getTagValue("site", eElement);
		 String Thrifthost = getTagValue("Thrifthost", eElement);
		 String oathsite = getTagValue("oathsite", eElement);
		 String username = getTagValue("username", eElement);
		 String password = getTagValue("password", eElement);
		 String libsite = getTagValue("libsite", eElement);
		 JSONObject envInfo = new JSONObject();
		 envInfo.put("site", site);
		 envInfo.put("Thrifthost", Thrifthost);
		 envInfo.put("oathsite", oathsite);
		 envInfo.put("username", username);
		 envInfo.put("password", password);
		 envInfo.put("libsite", libsite);
		 
		return envInfo;
	}
	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
	 
	        Node nValue = (Node) nlList.item(0);
	 
		return nValue.getNodeValue();
	}

}
