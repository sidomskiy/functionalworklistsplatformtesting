package com.iii.common;
/**

 * @version $Id: ThriftService.java 245983 2015-04-06 22:43:57Z iii $ $Rev: 232046 $ $Date: 2014-04-25 12:32:28 -0700 (Fri, 25 Apr 2014) $

 */
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TMultiplexedProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;





public class ThriftService {
	
	// get thrift service by name
	public static TProtocol getThriftService(String serviceName) throws TTransportException {
		TTransport transport = new TSocket("archdev.iii.com", 1037);
		//"archdev.iii.com", 1037
		// "dev-sierra-app01.iii.com", 5020
		transport.open();
		TProtocol protocol = new  TBinaryProtocol(transport);
		
		// Wrap the specified protocol, allowing it to be used to communicate with a multiplexing server
		return new TMultiplexedProtocol(protocol, serviceName);
		
	}

}
