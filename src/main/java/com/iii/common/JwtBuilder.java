package com.iii.common;
/**

 * @version $Id: JwtBuilder.java 245398 2015-03-19 15:25:49Z iii $ $Rev: 244203 $ $Date: 2015-02-04 15:18:26 -0800 (Wed, 04 Feb 2015) $

 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import com.iii.common.RandomStringUUID;
import org.joda.time.DateTimeZone;
import org.joda.time.DateTime;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;


public class JwtBuilder {
	
	static String uuid = "";
	static int ts = 0;
	static String encIii = "";
	static int cid = 1;
	static int exptime = 180;
	
	private JwtBuilder(){
		
		
	}
		
	
	/**
	 * Creating default JWT header ( random UUID, 3 min exp time, worklists app):
	 * String jwtheader = "IIIJWT" + JwtBuilder.generateToken();
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String generateToken() throws NoSuchAlgorithmException, CertificateException, IOException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
		JwtBuilder myIIIJWT = new JwtBuilder();
		String jwt = "";
		uuid = RandomStringUUID.createClientUuid();
		DateTime dt = new DateTime ( DateTimeZone.UTC); 
		ts = (int) (dt.getMillis()/1000L);
		SecretKey secretKey = generateHmacKey();
		encIii = myIIIJWT.encodeIii(secretKey);
		
		jwt = myIIIJWT.sign(secretKey);
		
		return "IIIJWT " + jwt;
	}
	
	/**
	 * Creating JWT header (3 min exp time, for specific uuid and app name � �worklists� or �patron� ):
	 * String uuid = "1d8cf412-8fca-4b84-bf22-b93737a4bffe";
	 * String cid = "worklists";
	 * String jwtheader = "IIIJWT" + JwtBuilder.generateToken(uuid, cid);
	 * String jwtheader = "IIIJWT" + JwtBuilder.generateToken();
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String generateToken(String nuuid, String cname) throws NoSuchAlgorithmException, CertificateException, IOException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
		JwtBuilder myIIIJWT = new JwtBuilder();
		String jwt = "";
		uuid = nuuid;
		cid = myIIIJWT.getCid(cname);
		DateTime dt = new DateTime ( DateTimeZone.UTC); 
		ts = (int) (dt.getMillis()/1000L);
		SecretKey secretKey = generateHmacKey();
		encIii = myIIIJWT.encodeIii(secretKey);
		jwt = myIIIJWT.sign(secretKey);
		
		return "IIIJWT " + jwt;
	}
	
	/**
	 * Creating JWT header ( for provided UUID,  expTimeInMinutes, app name � �worklists� or �patron� ):
	 * String uuid = "1d8cf412-8fca-4b84-bf22-b93737a4bffe";
	 * String cid = "worklists";
	 * int expTimeInMinutes= 25;
	 * String jwtheader = "IIIJWT" + JwtBuilder.generateToken(uuid, cid, expTimeInMinutes);;
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String generateToken(String nuuid, String cname, int expTimeInMinutes) throws NoSuchAlgorithmException, CertificateException, IOException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
		JwtBuilder myIIIJWT = new JwtBuilder();
		exptime = expTimeInMinutes * 60;
		String jwt = "";
		uuid = nuuid;
		cid = myIIIJWT.getCid(cname);
		DateTime dt = new DateTime ( DateTimeZone.UTC); 
		ts = (int) (dt.getMillis()/1000L);
		SecretKey secretKey = generateHmacKey();
		encIii = myIIIJWT.encodeIii(secretKey);
		jwt = myIIIJWT.sign(secretKey);
		
		return "IIIJWT " + jwt;
	}
	
	private  String sign(SecretKey secretKey) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
		String signingInput = createSigningInput();
		String signed = signHmac(signingInput, secretKey);
		String s = signingInput + "." + signed;
		return s;
	}
	
	private  String createSigningInput() throws UnsupportedEncodingException{
		
		JSONObject header=new JSONObject();
		header.put("typ", "JWT");
		header.put("alg", "HS512");
		byte[] bheader =(header.toString()).getBytes("UTF-8");
		String b64Header = Base64.encodeBase64String(bheader);
		JSONObject jClaims = new JSONObject();
		jClaims.put("iss", uuid);
		jClaims.put("iat", ts);
		jClaims.put("exp", ts+exptime);
		jClaims.put("iii", encIii);
		jClaims.put("cid", cid);
		byte[] claimsJ = (jClaims.toString()).getBytes("UTF-8");
		String b64Claims = Base64.encodeBase64String(claimsJ);
		String s = b64Header + "." + b64Claims;
		return s;
	}
	
	private  String signHmac(String signingInput, SecretKey secretKey) throws NoSuchAlgorithmException, InvalidKeyException, IllegalStateException, UnsupportedEncodingException{
		Mac mac = Mac.getInstance("HmacSHA512");
		mac.init(secretKey);
		
		String signed = Base64.encodeBase64String(mac.doFinal(signingInput.getBytes("UTF-8")));
		return signed;
	}
	
	private  String encodeIii(SecretKey secretKey) throws CertificateException, IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		
		PublicKey pk = getPublicKeyFromCertificate();
		Cipher rsaCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		rsaCipher.init(Cipher.ENCRYPT_MODE, pk);
		byte[] encryptedData = rsaCipher.doFinal(secretKey.getEncoded());
		String iiiEncData = Base64.encodeBase64String(encryptedData);
		return iiiEncData;
	}
	
	private  PublicKey getPublicKeyFromCertificate() throws CertificateException, IOException{
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		 
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream file = classLoader.getResourceAsStream("rsacert.cer");
		
		
        Certificate cert = cf.generateCertificate(file);
        file.close();
		return cert.getPublicKey();
	}
	
	private static SecretKey generateHmacKey() throws NoSuchAlgorithmException, UnsupportedEncodingException{
		SecretKey key = KeyGenerator.getInstance("HmacSHA512").generateKey(); 
		return key;
	}
	
	private  int getCid (String cname) {
		if (cname == "worklists"){
			cid = 1;
		}else if (cname == "patron"){
			cid = 2;
		}else if (cname == "innreach"){
			cid = 3;
		}else if (cname == "kb"){
			cid = 4;
		}else if (cname == "analytics"){
			cid = 6;
		}else {
			cid = 1;
		}
		
		return cid;
	}
	
	

}
