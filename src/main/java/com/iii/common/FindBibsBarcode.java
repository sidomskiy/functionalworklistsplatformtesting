package com.iii.common;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

public class FindBibsBarcode {
	
	@Test
	public static String test(String testSite) throws Exception {
		JSONObject getEnvInfo = UtilityTools.getEnvInfo(testSite);
		String site = getEnvInfo.getString("site");
		String Thrifthost = getEnvInfo.getString("Thrifthost");
		String oathsite = getEnvInfo.getString("oathsite");
		String username = getEnvInfo.getString("username");
		String password = getEnvInfo.getString("password");
		String site_id = getEnvInfo.getString("libsite");
		String bibIds ="";
		String barcode ="";
		String requestUrl = "https://" + Thrifthost + "/iii/sierra-api/v5/internal/bibs/search?limit=20&offset=1000&materialType=a&text=*";
		
		String responseStr = UtilityTools.streamStringAPIUrl(requestUrl, testSite);
		
		
		JSONObject RespObj = new JSONObject(responseStr);
		
		JSONArray RespArr = RespObj.getJSONArray("entries");
		
		for (int i=0; i < RespArr.length(); i++){
			JSONObject bibInfo = RespArr.getJSONObject(i).getJSONObject("bib");
			String bibId = bibInfo.getString("id");
			if (bibIds.length() == 0){
				bibIds = bibId;
			}else {
				bibIds = bibIds + "%2C" + bibId;
			}
			
			
		}
		
		
		
		String requestUrls = "https://" + Thrifthost + "/iii/sierra-api/v5/items/?bibIds=" + bibIds;
		String responseStrs = UtilityTools.streamStringAPIUrl(requestUrls, testSite);
		
		JSONObject ItemObj = new JSONObject(responseStrs);
		JSONArray ItemsArr = ItemObj.getJSONArray("entries");
		for (int j=0; j < ItemsArr.length(); j++){
			JSONObject ItemObject = ItemsArr.getJSONObject(j);
			String bar = ItemObject.getString("barcode");
			
			if (barcode.length() == 0){
				barcode = "\"" + bar + "\"";
			}else {
				barcode = barcode + ", \"" + bar + "\"";
			}
		}
		
		
		return barcode;
	}
	
	public static String singleBarcode(String testSite) throws Exception {
		JSONObject getEnvInfo = UtilityTools.getEnvInfo(testSite);
		String site = getEnvInfo.getString("site");
		String Thrifthost = getEnvInfo.getString("Thrifthost");
		String oathsite = getEnvInfo.getString("oathsite");
		String username = getEnvInfo.getString("username");
		String password = getEnvInfo.getString("password");
		String site_id = getEnvInfo.getString("libsite");
		String bibIds ="";
		String barcode ="";
		
		Random rand = new Random();
		int  n = rand.nextInt(1500) + 1000;
		
		String requestIdUrl = "https://" + Thrifthost + "/iii/sierra-api/v5/internal/bibs/search?limit=1&materialType=a&offset=" + n +"&text=*";
		String responseIdStr = UtilityTools.streamStringAPIUrl(requestIdUrl, testSite);
		
		JSONObject RespIdObj = new JSONObject(responseIdStr);
		JSONArray RespIdArr = RespIdObj.getJSONArray("entries");
		JSONObject bibInfo = RespIdArr.getJSONObject(0).getJSONObject("bib");
		String bibId = bibInfo.getString("id");
		
		// -----------------------------
		String requestBibUrl = "https://" + Thrifthost + "/iii/sierra-api/v5/items/?bibIds=" + bibId;
		String responseBibStr = UtilityTools.streamStringAPIUrl(requestBibUrl, testSite);
		
		JSONObject ItemObj = new JSONObject(responseBibStr);
		JSONArray ItemsArr = ItemObj.getJSONArray("entries");
		JSONObject ItemObject = ItemsArr.getJSONObject(1);
		barcode = ItemObject.getString("barcode");
		
		
		return barcode;
	}

}
