package com.iii.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.iii.common.UtilityTools;

public class ShowLibrary {
	
	static WebDriver driver;
	static long sleepTime = 1000;
	
	@FindBy(how = How.LINK_TEXT, using = "New Library")
    public static WebElement newLibrary;
	
	@FindBy(how = How.LINK_TEXT, using = "Edit")
    public static WebElement edit;
	
	@FindBy(how = How.NAME, using = "_action_delete")
    public static WebElement delete;
	
	public ShowLibrary(WebDriver driver)
    {
        ShowLibrary.driver = driver;
    }
	
	
	public static void delete(){
		UtilityTools.MySleep(sleepTime);
		delete.click();
		UtilityTools.MySleep(sleepTime);
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
	}

}
