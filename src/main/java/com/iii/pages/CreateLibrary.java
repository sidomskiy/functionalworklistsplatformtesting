package com.iii.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.iii.common.UtilityTools;

public class CreateLibrary {
	
	static WebDriver driver;
	static long sleepTime = 1000;
	
	@FindBy(how = How.ID, using = "siteCode")
    public static WebElement siteCode;
	
	@FindBy(how = How.ID, using = "name")
    public static WebElement name;
	@FindBy(how = How.ID, using = "siteURL")
    public static WebElement siteURL;
	@FindBy(how = How.ID, using = "thriftHost")
    public static WebElement thriftHost;
	@FindBy(how = How.ID, using = "authenticationThriftPort")
    public static WebElement authenticationThriftPort;
	@FindBy(how = How.ID, using = "create")
    public static WebElement create;
	
	
	public CreateLibrary(WebDriver driver)
    {
        CreateLibrary.driver = driver;
    }
	
	public static void creeateLibrary(String SiteCode, String Name, String SiteUrl, String ThriftHost, String AuthTriftPort){
		siteCode.sendKeys(SiteCode);
		name.sendKeys(Name);
		siteURL.sendKeys(SiteUrl);
		thriftHost.sendKeys(ThriftHost);
		authenticationThriftPort.sendKeys(AuthTriftPort);
		
		create.click();
	}

}
