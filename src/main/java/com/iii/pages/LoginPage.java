package com.iii.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.iii.common.UtilityTools;

public class LoginPage {
	
	static WebDriver driver;
	static long sleepTime = 1000;
	
	@FindBy(how = How.ID, using = "username")
    public static WebElement Username;
	
	@FindBy(how = How.ID, using = "password")
    public static WebElement uPassword;
	
	@FindBy(how = How.ID, using = "submit")
    public static WebElement loginSubmit;
	
	public LoginPage(WebDriver driver)
    {
        LoginPage.driver = driver;
    }
	
	public static void login(String UserName, String Password){
		
		Username.sendKeys(UserName);
		UtilityTools.MySleep(sleepTime);
		uPassword.sendKeys(Password);
		
		loginSubmit.click();
		UtilityTools.MySleep(sleepTime);
		
		
	}

}
